#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## MODELO CBA SESI
## Autor: Luis Felipe Riehs Camargo
## Version: 2.0
## Date: 04/Jun/2018
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

require(shinydashboard)
require(shiny)
require(shinyjs)
require(readxl)
require(dplyr)
require(tidyr)
require(lubridate)
require(matrixStats)
require(knitr)
require(kableExtra)
require(pander)
require(pastecs)
require(rmarkdown)
require(yaml)
require(knitr)

shinyUI(dashboardPage(
  skin = "blue",
  title = "Calculadora Sistêmica",
  
  dashboardHeader(title = HTML(paste(icon('bar-chart'),'Calculadora Sistêmica SESI'))),
  
  dashboardSidebar(
    sidebarMenu(
      
      id = "tabs",
      menuItem("Dados: Situação Atual",tabName = "data_asis", icon = icon("database")),
      menuItem("Dados: Iniciativas",tabName = "data_iniciativa", icon = icon("database")),
      menuItem("Observar os Resultados",tabName = "results", icon = icon("bar-chart")),
      menuItem("Download template dados entrada",tabName = "arquivosoriginais", icon = icon("database"))
    )
    
  ), # END dashboardSidebar
  
  dashboardBody(
    shinyjs::useShinyjs(),
    tabItems(
      tabItem("data_asis",
              box(width = 12,title = 'Importar as Informações da Situação Atual da Empresa', solidHeader = T, status = 'primary',
                  # Carregar arquivo Excel
                  fileInput('file1', 'Selecione o arquivo de dados em formato Excel (.xls, .xlsx) ou csv:',
                            accept=c('text/csv', 'text/comma-separated-values,text/plain', '.csv', '.xlsx', '.xls'))   
              ),
              box(width = 12,title = 'Logs de erros', solidHeader = T, status = 'warning',
                  DT::dataTableOutput("outputTable_AsisLogs")
              ),
              box(width = 4,title = 'Baixar arquivo pdf com análise inicial dos dados inseridos', solidHeader = T, status = 'primary',
                  actionButton("makepdf1", "Criar PDF"),
                  downloadButton('downloadData1', "Baixar")
              ),
              box(width = 4,title = 'Baixar arquivo csv com número de eventos coletados', solidHeader = T, status = 'primary',
                  downloadButton('downloadData1b', "Baixar")
              ),
              box(width = 4,title = 'Baixar arquivo csv com prob. de eventos coletados', solidHeader = T, status = 'primary',
                  downloadButton('downloadData1c', "Baixar")
              )

      ), # END tabItem "data_asis"
      
      tabItem("data_iniciativa",
              box(width = 8, title = 'Importar as Informações Sobre as Iniciativas', solidHeader = T, status = 'primary',
                  # Carregar arquivo Excel INICIATIVA  
                  h4(strong('Importar Planilha de Dados das Inciativas')),
                  fileInput('file3', 'Selecione o arquivo de dados em formato Excel (.xls, .xlsx) ou csv:',
                            accept=c('text/csv', 'text/comma-separated-values,text/plain', '.csv', '.xlsx', '.xls')) 
              ),
              box(width = 4, title = 'Rodar a Calculadora', solidHeader = T, status = 'warning',
                  DT::dataTableOutput("outputTable_StatusCalc"),
                  h5('Atenção: Rodar a calculadora SOMENTE se ambos os logs estiverem sem erros'),
                  actionButton("RodarCalculadorabtn", "Rodar Calculadora")
              ),
              box(width = 8, title = 'Logs de erros planilha Iniciativas', solidHeader = T, status = 'warning',
                  DT::dataTableOutput("outputTable_INITLogs")
              )
          ), # END tabItem "data_iniciativa" 
      
      tabItem("results",
              box(width = 10, title = 'Síntese do Resultado da Calculadora', solidHeader = T, status = 'primary',
                  DT::dataTableOutput("output_calculadora")
              ),
              box(width = 10,title = 'Baixar arquivo pdf com os resultados da calculadora', solidHeader = T, status = 'primary',
                  h5("Baixar o Relatório em formato pdf:"),
                  downloadButton('downloadData2', "Baixar"),
                  h5("Baixar o Relatório em formato pdf com logs:"),
                  downloadButton('downloadData_logs', "Baixar")
              )
      ), # END tabItem "results" 
      
      tabItem("arquivosoriginais",
              box(width = 10,title = 'Baixar arquivo de entreda versão vigente', solidHeader = T, status = 'primary',
                  h5("Baixar arquivo excel template para preenchimento dados da empresa (Situação Atual)"),
                  downloadButton('excelasis', "Baixar"),
                  h5("Baixar arquivo excel template para preenchimento dados das iniciativas (Situação Futura)"),
                  downloadButton('excelinit', "Baixar")
              )
      ) # END tabItem "results" 
      

    ) # END tabItem
    
  ) # END dashboardBody
  
)) # END dashboardPage 
