#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## MODELO CBA SESI
## Autor: Luis Felipe Riehs Camargo
## Version: 2.0
## Date: 04/Jun/2018
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

## Carregar os pacotes necessários
require(shiny)
require(shinyjs)
require(readxl)
require(dplyr)
require(tidyr)
require(lubridate)
require(matrixStats)
require(knitr)
require(kableExtra)
require(pander)
require(pastecs)
require(rmarkdown)
require(yaml)
require(knitr)
# Instalação do pacote calculadora sistêmica (oshcba) - após instalado no computador não há necessidade de rodar novamente
# devtools::install_github("pedroliman/oshcba", auth_token = "a99dc0254a0e73b9e08fca868ab1df6e32877153")
require(oshcba) # Calculadora Sistêmica

## Shiny Server
shinyServer(function(input, output, session) {
  
  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  # Parte 1 Avaliação dados Situação Atual da Empresa (ASIS) -----
  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  parte1 <- reactive({
    
    #### 1.1 Carregar os Dados ####
    inFile <- input$file1
    ext <- tools::file_ext(inFile$name)
    file.rename(inFile$datapath,
                paste(inFile$datapath, ext, sep="."))

    #### 1.2 Ler Dados Planilha EXCEL Planilha ASIS (AS_IS.xlsx) ####

    ### Versão do arquivo de dados
    #+++++++++++++++++++++++++++++++++++++++++++++
    versao_planilha_asis = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                 range = "Parametrização!E3",
                                                 col_names = FALSE))
    
    ### 1.2.1 Ler Planilha Check Suporte
    #+++++++++++++++++++++++++++++++++++++++++++++
    dataset_check1 = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                           range = "Check_support!A1:D72",
                                           col_names = TRUE))
    
    ### 1.2.2 Ler Planilha de ASIS - PARAMETRIZAÇÃO
    #+++++++++++++++++++++++++++++++++++++++++++++
    ### Cadastro da Aplicação
    dataset_ASIS_param_cadastEmp = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                         range = "Parametrização!C7:C11",
                                                         col_names = FALSE))
    row.names(dataset_ASIS_param_cadastEmp) <- c("NomeEmpresa",
                                                 "CNPJempresa",
                                                 "CNAEempresa",
                                                 "AnalistaEmpresa",
                                                 "CPFanalista")
    DB_ASIS_param_cadastEmp = t(dataset_ASIS_param_cadastEmp)
    
    dataset_ASIS_param_cadastSesi = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                          range = "Parametrização!C15:C16",
                                                          col_names = FALSE))
    row.names(dataset_ASIS_param_cadastSesi) <- c("AnalistaSESI",
                                                  "CPFanalistaSSESI")
    DB_ASIS_param_cadastSesi = t(dataset_ASIS_param_cadastSesi)

    ### Versão utilizada da calculadora (Básica; Simplificada; Customizada)    
    versao_calculadora = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                               range = "Parametrização!C19",
                                               col_names = FALSE))
    names(versao_calculadora) <- c("versao_calculadora")

    ### Seleção de Módulos da Calculadora
    dataset_ASIS_param_Modulos = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                       range = "Parametrização!E26:E42",
                                                       col_names = FALSE))
    row.names(dataset_ASIS_param_Modulos) <- c("BeneficioFAP",
                                               "BeneficioMultas",
                                               "BeneficioAcoesRegressivasINSS",
                                               "BeneficioReabilitacao",
                                               "BeneficioPlanodeSaude",
                                               "BeneficioReclamatorias",
                                               "BeneficioDespesasMedicas",
                                               "BeneficioInterrupcaoAcidentes",
                                               "BeneficioInterdicaoFiscalizacao",
                                               "BeneficioSeguroPatrimonial",
                                               "BeneficioRefugoERetrabalho",
                                               "BeneficioMPInsumos",
                                               "BeneficioPresenteismo",
                                               "BeneficioGanhoQualidade",
                                               "BeneficioGanhoProdutividade",
                                               "BeneficioImagemContratacao",
                                               "BeneficioClima")
    names(dataset_ASIS_param_Modulos) <- c("Selecionado (1.sim 0.não)")
    names(dataset_ASIS_param_Modulos) <- c("X__1")
    
    DB_ASIS_param_Modulos = data.frame(t(dataset_ASIS_param_Modulos))
    
    ### Taxa de Desconto selecionada 
    dataset_ASIS_param_taxadesconto = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                            range = "Parametrização!C46",
                                                            col_names = FALSE))
    colnames(dataset_ASIS_param_taxadesconto) <- c("TaxaDeDesconto")
    
    ### 1.2.3 Ler Planilha de ASIS - SIMPLIFICADO - INSS
    #+++++++++++++++++++++++++++++++++++++++++++++
    dataset_ASIS_Simple_INSS = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                     sheet = "Simplificado_INSS",
                                                     skip = 4,
                                                     col_names = TRUE))

    ### 1.2.4 Ler Planilha de ASIS - SIMPLIFICADO - OUTROS
    #+++++++++++++++++++++++++++++++++++++++++++++
    dataset_ASIS_Simple_Outros = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                       sheet = "Simplificado_Outros",
                                                       skip = 5,
                                                       range = "A6:Q35",
                                                       col_names = TRUE))
    
    row.names(dataset_ASIS_Simple_Outros) <- c("CustoMDO",
                                               "CustoMedSubstitu",
                                               "DesligamentosInvoluntarios",
                                               "Aux_NroTotalDesligamentos",
                                               "Aux_TotalDiasAfast_Men15",
                                               "Funcionarios",
                                               "Aux_TotalHorasTrabalhadas",
                                               "HorasHomemExposicaoRisco",
                                               "Nev_Afmaior15_DoenOcup",
                                               "Nev_Afmaior15_NRelac",
                                               "Nev_Afmaior15_Tipico",
                                               "Nev_Afmaior15_Trajeto",
                                               "Nev_Afmenor15_DoenOcup",
                                               "Nev_Afmenor15_NRelac",
                                               "Nev_Afmenor15_Tipico",
                                               "Nev_Afmenor15_Trajeto",
                                               "Nev_Obito_DoenOcup",
                                               "Nev_Obito_NRelac",
                                               "Nev_Obito_Tipico",
                                               "Nev_Obito_Trajeto",
                                               "Nev_Safast_DoenOcup",
                                               "Nev_Safast_NRelac",
                                               "Nev_Safast_Tipico",
                                               "Nev_Safast_Trajeto",
                                               "Aux_NroTotalDias_Faltas",
                                               "TaxaFrequencia",
                                               "TaxaGravidade",
                                               "VarPIB",
                                               "DiasUteis")
    
    ### Criar Bando de Dados Histórico (BD_Histórico) e Banco de dados iniciativas (BD_Iniciativas)
    ### Seleciona somente colunas desejadas da Planilha Histórico
    DB_ASIS_Simple_Outros_Arbitrado = data.frame( dplyr::select(dataset_ASIS_Simple_Outros, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Simple_Outros_Observado = data.frame( dplyr::select(dataset_ASIS_Simple_Outros, dplyr::contains("X2")))
    Flag_Modulos = rep(1, 29)# Flag sinalizando variáveis obrigaórias (1)
    DB_ASIS_Simple_Outros_Arbitrado = cbind(DB_ASIS_Simple_Outros_Arbitrado, Flag_Modulos)
    DB_ASIS_Simple_Outros_Observado = cbind(DB_ASIS_Simple_Outros_Observado, Flag_Modulos)
    
    ## Utilizado para os calculos das prob. dos eventos (DESLOCAR PARA SECAO DE TRATAMENTO)
    DB_ASIS_Simple_Outros_Arbitrado_eventos = DB_ASIS_Simple_Outros_Arbitrado[9:25,] # Seleção dos eventos arbitrados pelo usuário
    DB_ASIS_Simple_Outros_Observado_eventos = DB_ASIS_Simple_Outros_Observado[9:25,] # Seleção dos eventos informados pelo usuário
    DB_ASIS_Simple_Outros_Observado_nrofunc = DB_ASIS_Simple_Outros_Observado[6,] # (dado utilizado para calculo das prob.)

    ### 1.2.5 Ler Planilha de ASIS - Simplificado_FAP
    #+++++++++++++++++++++++++++++++++++++++++++++
    dataset_ASIS_Simp_FAP = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                  range = "Simplificado_FAP!E6:N19",
                                                  col_names = TRUE))
    
    row.names(dataset_ASIS_Simp_FAP) <- c("CustoTotalBeneficiosFAP",
                                          "FolhadePagamento",
                                          "FuncionariosFAP",
                                          "Indicecusto",
                                          "Indicefrequencia",
                                          "Indicegravidade",
                                          "IndiceCompostoFAP",
                                          "Percentilcusto",
                                          "Percentilfrequencia",
                                          "Percentilgravidade",
                                          "RATAjustado",
                                          "RATTabela",
                                          "TurnoverGeral")
    
    DB_ASIS_Custom_Observado_1 = data.frame(select(dataset_ASIS_Simp_FAP, dplyr::contains("X2")))
    DB_ASIS_Custom_Arbitrado_1 = DB_ASIS_Custom_Observado_1[, 1:3]
    colnames(DB_ASIS_Custom_Arbitrado_1) <- c("Usual", "Máximo", "Mínimo")
    
    if(DB_ASIS_param_Modulos$BeneficioFAP == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 13) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_1 = cbind(DB_ASIS_Custom_Arbitrado_1, Flag_Modulos)
      DB_ASIS_Custom_Observado_1 = cbind(DB_ASIS_Custom_Observado_1, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 13) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_1 = cbind(DB_ASIS_Custom_Arbitrado_1, Flag_Modulos)
      DB_ASIS_Custom_Observado_1 = cbind(DB_ASIS_Custom_Observado_1, Flag_Modulos)
    }

    ### 1.2.6 Ler Planilha de ASIS - Custom_ReduçõesFiscais
    #+++++++++++++++++++++++++++++++++++++++++++++
    dataset_ASIS_multas = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                range = "Custom_ReduçõesFiscais!E8:Q18",
                                                col_names = TRUE))
    
    row.names(dataset_ASIS_multas) <- c("DespesaExposicaoMulta1",
                                        "DespesaExposicaoMulta2",
                                        "DespesaExposicaoMulta3",
                                        "DespesaExposicaoMulta4",
                                        "DespesaExposicaoMulta5",
                                        "Multas1",
                                        "Multas2",
                                        "Multas3",
                                        "Multas4",
                                        "Multas5")
    
    DB_ASIS_Custom_Arbitrado_2 = data.frame(select(dataset_ASIS_multas, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_2 = data.frame(select(dataset_ASIS_multas, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioMultas == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 10) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_2 = cbind(DB_ASIS_Custom_Arbitrado_2, Flag_Modulos)
      DB_ASIS_Custom_Observado_2 = cbind(DB_ASIS_Custom_Observado_2, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 10) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_2 = cbind(DB_ASIS_Custom_Arbitrado_2, Flag_Modulos)
      DB_ASIS_Custom_Observado_2 = cbind(DB_ASIS_Custom_Observado_2, Flag_Modulos)
    }

    ### 1.2.7 Ler Planilha de ASIS - Custom_DespesasEvitáveis
    #+++++++++++++++++++++++++++++++++++++++++++++
    ### Dimensão: BeneficioReclamatorias
    dataset_ASIS_Reclamatorias = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                       range = "Custom_DespesasEvitáveis!E8:Q10",
                                                       col_names = TRUE))
    
    row.names(dataset_ASIS_Reclamatorias) <- c("CustoMedioReclamatorias",
                                               "PReclamatoria")
    
    DB_ASIS_Custom_Arbitrado_3 = data.frame(select(dataset_ASIS_Reclamatorias, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_3 = data.frame(select(dataset_ASIS_Reclamatorias, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioReclamatorias == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 2) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_3 = cbind(DB_ASIS_Custom_Arbitrado_3, Flag_Modulos)
      DB_ASIS_Custom_Observado_3 = cbind(DB_ASIS_Custom_Observado_3, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 2) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_3 = cbind(DB_ASIS_Custom_Arbitrado_3, Flag_Modulos)
      DB_ASIS_Custom_Observado_3 = cbind(DB_ASIS_Custom_Observado_3, Flag_Modulos)
    }
    

    ### Dimensão: BeneficioAcoesRegressivasINSS
    dataset_ASIS_AcoesRegressivasINSS = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                              range = "Custom_DespesasEvitáveis!E15:Q16",
                                                              col_names = TRUE))
    
    row.names(dataset_ASIS_AcoesRegressivasINSS) <- c("Aux_NroAcoesRegre")
    
    DB_ASIS_Custom_Arbitrado_4 = data.frame(select(dataset_ASIS_AcoesRegressivasINSS, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_4 = data.frame(select(dataset_ASIS_AcoesRegressivasINSS, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioAcoesRegressivasINSS == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 1) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_4 = cbind(DB_ASIS_Custom_Arbitrado_4, Flag_Modulos)
      DB_ASIS_Custom_Observado_4 = cbind(DB_ASIS_Custom_Observado_4, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 1) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_4 = cbind(DB_ASIS_Custom_Arbitrado_4, Flag_Modulos)
      DB_ASIS_Custom_Observado_4 = cbind(DB_ASIS_Custom_Observado_4, Flag_Modulos)
    }

    ### Dimensão: BeneficioDespesasMedicas
    dataset_ASIS_DespesasMedicas = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                         range = "Custom_DespesasEvitáveis!E21:Q22",
                                                         col_names = TRUE))
    
    row.names(dataset_ASIS_DespesasMedicas) <- c("Aux_DespesaMedicaTotal")
    
    DB_ASIS_Custom_Arbitrado_5 = data.frame(select(dataset_ASIS_DespesasMedicas, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_5 = data.frame(select(dataset_ASIS_DespesasMedicas, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioDespesasMedicas == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 1) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_5 = cbind(DB_ASIS_Custom_Arbitrado_5, Flag_Modulos)
      DB_ASIS_Custom_Observado_5 = cbind(DB_ASIS_Custom_Observado_5, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 1) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_5 = cbind(DB_ASIS_Custom_Arbitrado_5, Flag_Modulos)
      DB_ASIS_Custom_Observado_5 = cbind(DB_ASIS_Custom_Observado_5, Flag_Modulos)
    }
    
    ### Dimensão: BeneficioPlanodeSaude
    dataset_ASIS_PlanodeSaude = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                      range = "Custom_DespesasEvitáveis!E27:Q28",
                                                      col_names = TRUE))
    
    row.names(dataset_ASIS_PlanodeSaude) <- c("DespesasPlanoInicial")
    
    DB_ASIS_Custom_Arbitrado_6 = data.frame(select(dataset_ASIS_PlanodeSaude, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_6 = data.frame(select(dataset_ASIS_PlanodeSaude, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioPlanodeSaude == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 1) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_6 = cbind(DB_ASIS_Custom_Arbitrado_6, Flag_Modulos)
      DB_ASIS_Custom_Observado_6 = cbind(DB_ASIS_Custom_Observado_6, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 1) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_6 = cbind(DB_ASIS_Custom_Arbitrado_6, Flag_Modulos)
      DB_ASIS_Custom_Observado_6 = cbind(DB_ASIS_Custom_Observado_6, Flag_Modulos)
    }
    
    ### Dimensão: BeneficioInterrupcaoAcidentes
    dataset_ASIS_InterrupcaoAcidentes = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                              range = "Custom_DespesasEvitáveis!E33:Q37",
                                                              col_names = TRUE))
    
    row.names(dataset_ASIS_InterrupcaoAcidentes) <- c("DiasInterrupcaoAcidenteObito",
                                                      "DiasInterrupcaoAcidenteOutros",
                                                      "Aux_LucroCessanteTotal_Obitos",
                                                      "Aux_LucroCessanteTotal_OutrosAcid")
    
    DB_ASIS_Custom_Arbitrado_7 = data.frame(select(dataset_ASIS_InterrupcaoAcidentes, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_7 = data.frame(select(dataset_ASIS_InterrupcaoAcidentes, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioInterrupcaoAcidentes == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 4) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_7 = cbind(DB_ASIS_Custom_Arbitrado_7, Flag_Modulos)
      DB_ASIS_Custom_Observado_7 = cbind(DB_ASIS_Custom_Observado_7, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 4) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_7 = cbind(DB_ASIS_Custom_Arbitrado_7, Flag_Modulos)
      DB_ASIS_Custom_Observado_7 = cbind(DB_ASIS_Custom_Observado_7, Flag_Modulos)
    }
    
    ### Dimensão: BeneficioInterdicaoFiscalizacao
    dataset_ASIS_InterdicaoFiscalizacao = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                                range = "Custom_DespesasEvitáveis!E42:Q44",
                                                                col_names = TRUE))
    
    row.names(dataset_ASIS_InterdicaoFiscalizacao) <- c("EventoInterdicao",
                                                        "LucroCessanteInterdicaoFiscalizacao")
    
    DB_ASIS_Custom_Arbitrado_8 = data.frame(select(dataset_ASIS_InterdicaoFiscalizacao, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_8 = data.frame(select(dataset_ASIS_InterdicaoFiscalizacao, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioInterdicaoFiscalizacao == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 2) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_8 = cbind(DB_ASIS_Custom_Arbitrado_8, Flag_Modulos)
      DB_ASIS_Custom_Observado_8 = cbind(DB_ASIS_Custom_Observado_8, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 2) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_8 = cbind(DB_ASIS_Custom_Arbitrado_8, Flag_Modulos)
      DB_ASIS_Custom_Observado_8 = cbind(DB_ASIS_Custom_Observado_8, Flag_Modulos)
    }
    
    ### Dimensão: BeneficioReabilitacao
    dataset_ASIS_Reabilitacao = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                      range = "Custom_DespesasEvitáveis!E49:Q51",
                                                      col_names = TRUE))
    
    row.names(dataset_ASIS_Reabilitacao) <- c("CustoMedioReabilitacao",
                                              "Aux_NroTotalReabilitados")
    
    DB_ASIS_Custom_Arbitrado_9 = data.frame(select(dataset_ASIS_Reabilitacao, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_9 = data.frame(select(dataset_ASIS_Reabilitacao, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioReabilitacao == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 2) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_9 = cbind(DB_ASIS_Custom_Arbitrado_9, Flag_Modulos)
      DB_ASIS_Custom_Observado_9 = cbind(DB_ASIS_Custom_Observado_9, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 2) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_9 = cbind(DB_ASIS_Custom_Arbitrado_9, Flag_Modulos)
      DB_ASIS_Custom_Observado_9 = cbind(DB_ASIS_Custom_Observado_9, Flag_Modulos)
    }
    
    ### Dimensão: BeneficioSeguroPatrimonial // DImensão não necessita de dado de entrada ASIS - Forçado que sempre será NULL os respectrivos objetos
    if(DB_ASIS_param_Modulos$BeneficioSeguroPatrimonial == 1){ # Carregar somente se o usuário selecionou o módulo
      DB_ASIS_Custom_Arbitrado_10 <- NULL
      DB_ASIS_Custom_Observado_10 <- NULL
      
    } else {c(DB_ASIS_Custom_Arbitrado_10<- NULL, DB_ASIS_Custom_Observado_10 <- NULL)}
    
    ### 1.2.8 Ler Planilha de ASIS - Custom_MelhorUsoRecursos
    #+++++++++++++++++++++++++++++++++++++++++++++
    ### Dimensão: BeneficioPresenteismo
    dataset_ASIS_Presenteismo = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                      range = "Custom_MelhorUsoRecursos!E8:Q9",
                                                      col_names = TRUE))
    
    row.names(dataset_ASIS_Presenteismo) <- c("PercPresenteismo")
    
    DB_ASIS_Custom_Arbitrado_11 = data.frame(select(dataset_ASIS_Presenteismo, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_11 = data.frame(select(dataset_ASIS_Presenteismo, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioPresenteismo == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 1) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_11 = cbind(DB_ASIS_Custom_Arbitrado_11, Flag_Modulos)
      DB_ASIS_Custom_Observado_11 = cbind(DB_ASIS_Custom_Observado_11, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 1) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_11 = cbind(DB_ASIS_Custom_Arbitrado_11, Flag_Modulos)
      DB_ASIS_Custom_Observado_11 = cbind(DB_ASIS_Custom_Observado_11, Flag_Modulos)
    }
    
    ### Dimensão: BeneficioMPInsumos
    dataset_ASIS_MPInsumos = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                   range = "Custom_MelhorUsoRecursos!E14:Q15",
                                                   col_names = TRUE))
    
    row.names(dataset_ASIS_MPInsumos) <- c("Aux_DespTotal_MPeInsumos")
    
    DB_ASIS_Custom_Arbitrado_12 = data.frame(select(dataset_ASIS_MPInsumos, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_12 = data.frame(select(dataset_ASIS_MPInsumos, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioMPInsumos == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 1) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_12 = cbind(DB_ASIS_Custom_Arbitrado_12, Flag_Modulos)
      DB_ASIS_Custom_Observado_12 = cbind(DB_ASIS_Custom_Observado_12, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 1) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_12 = cbind(DB_ASIS_Custom_Arbitrado_12, Flag_Modulos)
      DB_ASIS_Custom_Observado_12 = cbind(DB_ASIS_Custom_Observado_12, Flag_Modulos)
    }
    
    ### Dimensão: BeneficioRefugoERetrabalho
    dataset_ASIS_RefugoERetrabalho = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                           range = "Custom_MelhorUsoRecursos!E20:Q21",
                                                           col_names = TRUE))
    
    row.names(dataset_ASIS_RefugoERetrabalho) <- c("Aux_DespTotal_RefugoeRetrabalho")
    
    DB_ASIS_Custom_Arbitrado_13 = data.frame(select(dataset_ASIS_RefugoERetrabalho, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_13 = data.frame(select(dataset_ASIS_RefugoERetrabalho, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioRefugoERetrabalho == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 1) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_13 = cbind(DB_ASIS_Custom_Arbitrado_13, Flag_Modulos)
      DB_ASIS_Custom_Observado_13 = cbind(DB_ASIS_Custom_Observado_13, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 1) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_13 = cbind(DB_ASIS_Custom_Arbitrado_13, Flag_Modulos)
      DB_ASIS_Custom_Observado_13 = cbind(DB_ASIS_Custom_Observado_13, Flag_Modulos)
    }
    
    ### 1.2.9 Ler Planilha de ASIS - Custom_Intangível
    #+++++++++++++++++++++++++++++++++++++++++++++
    ### Dimensão: BeneficioImagemContratacao
    dataset_ASIS_ImagemContratacao = data.frame(read_excel(paste(inFile$datapath, ext, sep="."), 
                                                           range = "Custom_Intangível!E8:Q10",
                                                           col_names = TRUE))
    
    row.names(dataset_ASIS_ImagemContratacao) <- c("TempoContratacaoPadrao",
                                                   "Aux_CustoContratacaoUmFuncionario")
    
    DB_ASIS_Custom_Arbitrado_14 = data.frame(select(dataset_ASIS_ImagemContratacao, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_14 = data.frame(select(dataset_ASIS_ImagemContratacao, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioImagemContratacao == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 2) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_14 = cbind(DB_ASIS_Custom_Arbitrado_14, Flag_Modulos)
      DB_ASIS_Custom_Observado_14 = cbind(DB_ASIS_Custom_Observado_14, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 2) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_14 = cbind(DB_ASIS_Custom_Arbitrado_14, Flag_Modulos)
      DB_ASIS_Custom_Observado_14 = cbind(DB_ASIS_Custom_Observado_14, Flag_Modulos)
    }
    
    ### Dimensão: BeneficioClima
    dataset_ASIS_Clima = data.frame(read_excel(paste(inFile$datapath, ext, sep="."),
                                               range = "Custom_Intangível!E15:Q16",
                                               col_names = TRUE))
    row.names(dataset_ASIS_Clima) <- c("DespSubstVolunt")
    
    DB_ASIS_Custom_Arbitrado_15 = data.frame(select(dataset_ASIS_Clima, one_of(c("Usual", "Máximo" , "Mínimo" ))))
    DB_ASIS_Custom_Observado_15 = data.frame(select(dataset_ASIS_Clima, dplyr::contains("X2")))
    
    if(DB_ASIS_param_Modulos$BeneficioClima == 1){ # Carregar somente se o usuário selecionou o módulo
      Flag_Modulos = rep(1, 1) # Flag 1 quando módulo é utilizado
      DB_ASIS_Custom_Arbitrado_15 = cbind(DB_ASIS_Custom_Arbitrado_15, Flag_Modulos)
      DB_ASIS_Custom_Observado_15 = cbind(DB_ASIS_Custom_Observado_15, Flag_Modulos)
    } else {
      Flag_Modulos = rep(0, 1) # Flag 0 quando módulo não é utilizado
      DB_ASIS_Custom_Arbitrado_15 = cbind(DB_ASIS_Custom_Arbitrado_15, Flag_Modulos)
      DB_ASIS_Custom_Observado_15 = cbind(DB_ASIS_Custom_Observado_15, Flag_Modulos)
    }
    
    #### 1.3 Consolidação Bancos de Dados coletados ####    
    DB_ASIS_Completo_Arbitrado = rbind(DB_ASIS_Simple_Outros_Arbitrado, 
                                       DB_ASIS_Custom_Arbitrado_1,
                                       DB_ASIS_Custom_Arbitrado_2,
                                       DB_ASIS_Custom_Arbitrado_3,
                                       DB_ASIS_Custom_Arbitrado_4,
                                       DB_ASIS_Custom_Arbitrado_5,
                                       DB_ASIS_Custom_Arbitrado_6,
                                       DB_ASIS_Custom_Arbitrado_7,
                                       DB_ASIS_Custom_Arbitrado_8,
                                       DB_ASIS_Custom_Arbitrado_9,
                                       DB_ASIS_Custom_Arbitrado_10,
                                       DB_ASIS_Custom_Arbitrado_11,
                                       DB_ASIS_Custom_Arbitrado_12,
                                       DB_ASIS_Custom_Arbitrado_13,
                                       DB_ASIS_Custom_Arbitrado_14,
                                       DB_ASIS_Custom_Arbitrado_15)                                    
    
    DB_ASIS_Completo_Observado = rbind(DB_ASIS_Simple_Outros_Observado, 
                                       DB_ASIS_Custom_Observado_1,
                                       DB_ASIS_Custom_Observado_2,
                                       DB_ASIS_Custom_Observado_3,
                                       DB_ASIS_Custom_Observado_4,
                                       DB_ASIS_Custom_Observado_5,
                                       DB_ASIS_Custom_Observado_6,
                                       DB_ASIS_Custom_Observado_7,
                                       DB_ASIS_Custom_Observado_8,
                                       DB_ASIS_Custom_Observado_9,
                                       DB_ASIS_Custom_Observado_10,
                                       DB_ASIS_Custom_Observado_11,
                                       DB_ASIS_Custom_Observado_12,
                                       DB_ASIS_Custom_Observado_13,
                                       DB_ASIS_Custom_Observado_14,
                                       DB_ASIS_Custom_Observado_15)
    
    #### 1.4 Testes e Avaliação Inicial dos Dados Carregados ####    
    ## Join data. Retain only rows in both sets.
    # VarModelName = row.names(DB_ASIS_Completo_Observado)
    # DB_ASIS_Completo_Arbitrado = cbind(VarModelName, DB_ASIS_Completo_Arbitrado)
    # DB_ASIS_Completo_Arbitrado_Check1 = inner_join(dataset_check1, data.frame(DB_ASIS_Completo_Arbitrado), by = "VarModelName")
    # DB_ASIS_Completo_Observado = cbind(VarModelName, DB_ASIS_Completo_Observado)
    # DB_ASIS_Completo_Observado_Check1 = inner_join(dataset_check1, data.frame(DB_ASIS_Completo_Observado), by = "VarModelName")
    DB_ASIS_Completo_Arbitrado_Check1 = cbind(dataset_check1, DB_ASIS_Completo_Arbitrado)
    DB_ASIS_Completo_Observado_Check1 = cbind(dataset_check1, DB_ASIS_Completo_Observado)
    
    Flag_Modulos_All = DB_ASIS_Completo_Observado$Flag_Modulos 
    
    ## Transpor o banco de dados
    DB_ASIS_Completo_Observado$Flag_Modulos <- NULL
    DB_ASIS_Completo_Arbitrado$Flag_Modulos <- NULL
    
    DB_ASIS_Simple_Outros_Arbitrado_t = data.frame(t(DB_ASIS_Completo_Arbitrado))
    DB_ASIS_Simple_Outros_Observado_t = data.frame(t(DB_ASIS_Completo_Observado))
    
    ## Criar coluna com os anos no BD Observado
    anos_bd <- c(lubridate::year(Sys.Date())-10,
                 lubridate::year(Sys.Date())-9,
                 lubridate::year(Sys.Date())-8,
                 lubridate::year(Sys.Date())-7,
                 lubridate::year(Sys.Date())-6,
                 lubridate::year(Sys.Date())-5,
                 lubridate::year(Sys.Date())-4,
                 lubridate::year(Sys.Date())-3,
                 lubridate::year(Sys.Date())-2,
                 lubridate::year(Sys.Date())-1)
    
    DB_ASIS_Simple_Outros_Observado_t = cbind(anos_bd, DB_ASIS_Simple_Outros_Observado_t)
    
    DB_ASIS_Simple_Outros_Arbitrado_t_stats = rbind(DB_ASIS_Simple_Outros_Arbitrado_t, stat.desc(DB_ASIS_Simple_Outros_Arbitrado_t))
    DB_ASIS_Simple_Outros_Observado_t_stats = rbind(DB_ASIS_Simple_Outros_Observado_t, stat.desc(DB_ASIS_Simple_Outros_Observado_t))
    
    ### A. Log Erros
    #+++++++++++++++++++++++++++++++++++++++++++++
    # 1. Testar se dados INSS foram carregados (Data.de.Despacho.do.Benefício..DDB)
    if(mean(dataset_ASIS_Simple_INSS$Total.Pago.Projeção..R..) > 0) {dados_inss = "Dados INSS carregados e ok"} else {dados_inss = "Dados INSS não foram informados no arquivo de dados"}
    
    # 2. Dados preenchidos - sinalizar se existem dados preenchidos - sinalizar os erros
    check_variaveis1 = matrix(-99.99, nrow = length(DB_ASIS_Completo_Observado[,1]), ncol = 2)
    colnames(check_variaveis1) <- c("Variável", "Avaliaçao")
    
    DB_ASIS_Simple_Outros_Observado_t2 = DB_ASIS_Simple_Outros_Observado_t
    DB_ASIS_Simple_Outros_Observado_t2$anos_bd <- NULL
    
    for(j in 1:length(DB_ASIS_Completo_Observado[,1])){
      if(sum(!is.na(DB_ASIS_Simple_Outros_Arbitrado_t[,j])) > 0 | sum(!is.na(DB_ASIS_Simple_Outros_Observado_t2[,j])) > 0) {
        check_variaveis1[j,2] = 'Ok'; check_variaveis1[j,1] = DB_ASIS_Completo_Observado_Check1[j,2]} else {check_variaveis1[j,1] = DB_ASIS_Completo_Observado_Check1[j,2]; check_variaveis1[j,2] = DB_ASIS_Completo_Observado_Check1[j,4]}
    }
    
    check_variaveis1 = data.frame(cbind(check_variaveis1, Flag_Modulos_All))
    check_variaveis1 = dplyr::filter(check_variaveis1, Flag_Modulos_All == "1")
    check_variaveis1$Flag_Modulos_All <- NULL
    
    ### B. Prob. dos Eventos
    #+++++++++++++++++++++++++++++++++++++++++++++
    eventos_pdf = DB_ASIS_Simple_Outros_Observado_eventos[,5:10]
    
    eventos_pdf_prob = cbind( 
      DB_ASIS_Simple_Outros_Observado_eventos[,5]/DB_ASIS_Simple_Outros_Observado_nrofunc[,5],
      DB_ASIS_Simple_Outros_Observado_eventos[,6]/DB_ASIS_Simple_Outros_Observado_nrofunc[,6],
      DB_ASIS_Simple_Outros_Observado_eventos[,7]/DB_ASIS_Simple_Outros_Observado_nrofunc[,7],
      DB_ASIS_Simple_Outros_Observado_eventos[,8]/DB_ASIS_Simple_Outros_Observado_nrofunc[,8],
      DB_ASIS_Simple_Outros_Observado_eventos[,9]/DB_ASIS_Simple_Outros_Observado_nrofunc[,9],
      DB_ASIS_Simple_Outros_Observado_eventos[,10]/DB_ASIS_Simple_Outros_Observado_nrofunc[,10])
    
    eventos_pdf_Mean = rowMeans(eventos_pdf, na.rm = TRUE)
    eventos_pdf = cbind(eventos_pdf, eventos_pdf_Mean)
    
    eventos_pdf_prob_Mean = rowMeans(eventos_pdf_prob, na.rm = TRUE)
    eventos_pdf_prob = cbind(eventos_pdf_prob, eventos_pdf_prob_Mean)
    
    row.names(eventos_pdf) <- c("Afast. > 15d - Doença Ocup.",
                                "Afast. > 15d - Doença Não Rel. Trabalho",
                                "Afast. > 15d - Acidente Típico",
                                "Afast. > 15d - Acidente Trajeto",
                                "Afast. < 15d - Doença Ocupacional",
                                "Afast. < 15d - DDoença Não Rel. Trabalho",
                                "Afast. < 15d - Acidente Típico",
                                "Afast. < 15d - Acidente Trajeto",
                                "Óbitos - Doença Ocupacional",
                                "Óbitos - Doença Não Relacionada ao Trabalho",
                                "Óbitos - Acidente Típico",
                                "Óbitos - Acidente Trajeto",
                                "Eventos sem Afast. - Doença Ocupacional",
                                "Eventos sem Afast. - Doença Não Rel. Trabalho",
                                "Eventos sem Afast. - Acidente Típico",
                                "Eventos sem Afast. - Acidente Trajeto",
                                "Total de Dias em Falta sem Atestado")
    
    row.names(eventos_pdf_prob) <- row.names(eventos_pdf)

    colnames(eventos_pdf) <- c(lubridate::year(Sys.Date())-6,
                               lubridate::year(Sys.Date())-5,
                               lubridate::year(Sys.Date())-4,
                               lubridate::year(Sys.Date())-3,
                               lubridate::year(Sys.Date())-2,
                               lubridate::year(Sys.Date())-1,
                               "Média")
    
    colnames(eventos_pdf_prob) <- c(
      lubridate::year(Sys.Date())-6,
      lubridate::year(Sys.Date())-5,
      lubridate::year(Sys.Date())-4,
      lubridate::year(Sys.Date())-3,
      lubridate::year(Sys.Date())-2,
      lubridate::year(Sys.Date())-1,
      "Média")

    eventos_pdf_arb = rbind(
      if(!is.na(DB_ASIS_Simple_Outros_Arbitrado_eventos[1,1])){paste("O valor usual arbitrado foi: ", DB_ASIS_Simple_Outros_Arbitrado_eventos[1,1], ". O valor máximo e minímo foram: (",DB_ASIS_Simple_Outros_Arbitrado_eventos[1,2], ",", DB_ASIS_Simple_Outros_Arbitrado_eventos[1,3],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Simple_Outros_Arbitrado_eventos[3,1])){paste("O valor usual arbitrado foi: ", DB_ASIS_Simple_Outros_Arbitrado_eventos[3,1], ". O valor máximo e minímo foram: (",DB_ASIS_Simple_Outros_Arbitrado_eventos[3,2], ",", DB_ASIS_Simple_Outros_Arbitrado_eventos[3,3],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Simple_Outros_Arbitrado_eventos[5,1])){paste("O valor usual arbitrado foi: ", DB_ASIS_Simple_Outros_Arbitrado_eventos[5,1], ". O valor máximo e minímo foram: (",DB_ASIS_Simple_Outros_Arbitrado_eventos[5,2], ",", DB_ASIS_Simple_Outros_Arbitrado_eventos[5,3],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Simple_Outros_Arbitrado_eventos[7,1])){paste("O valor usual arbitrado foi: ", DB_ASIS_Simple_Outros_Arbitrado_eventos[7,1], ". O valor máximo e minímo foram: (",DB_ASIS_Simple_Outros_Arbitrado_eventos[7,2], ",", DB_ASIS_Simple_Outros_Arbitrado_eventos[7,3],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Simple_Outros_Arbitrado_eventos[11,1])){paste("O valor usual arbitrado foi:", DB_ASIS_Simple_Outros_Arbitrado_eventos[11,1])} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Simple_Outros_Arbitrado_eventos[17,1])){paste("O valor usual arbitrado foi: ", DB_ASIS_Simple_Outros_Arbitrado_eventos[17,1], ". O valor máximo e minímo foram: (",DB_ASIS_Simple_Outros_Arbitrado_eventos[17,2], ",", DB_ASIS_Simple_Outros_Arbitrado_eventos[17,3],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Completo_Arbitrado[48,2])){paste("O valor usual arbitrado foi: ", DB_ASIS_Completo_Arbitrado[48,2], ". O valor máximo e minímo foram: (",DB_ASIS_Completo_Arbitrado[48,3], ",", DB_ASIS_Completo_Arbitrado[48,4],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Completo_Arbitrado[49,2])){paste("O valor usual arbitrado foi: ", DB_ASIS_Completo_Arbitrado[49,2], ". O valor máximo e minímo foram: (",DB_ASIS_Completo_Arbitrado[49,3], ",", DB_ASIS_Completo_Arbitrado[49,4],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Completo_Arbitrado[50,2])){paste("O valor usual arbitrado foi: ", DB_ASIS_Completo_Arbitrado[50,2], ". O valor máximo e minímo foram: (",DB_ASIS_Completo_Arbitrado[50,3], ",", DB_ASIS_Completo_Arbitrado[50,4],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Completo_Arbitrado[51,2])){paste("O valor usual arbitrado foi: ", DB_ASIS_Completo_Arbitrado[51,2], ". O valor máximo e minímo foram: (",DB_ASIS_Completo_Arbitrado[51,3], ",", DB_ASIS_Completo_Arbitrado[51,4],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Completo_Arbitrado[52,2])){paste("O valor usual arbitrado foi: ", DB_ASIS_Completo_Arbitrado[52,2], ". O valor máximo e minímo foram: (",DB_ASIS_Completo_Arbitrado[52,3], ",", DB_ASIS_Completo_Arbitrado[52,4],")")} else {"Evento não arbitrado"},
      if(!is.na(DB_ASIS_Completo_Arbitrado[62,2])){paste("O valor usual arbitrado foi: ", DB_ASIS_Completo_Arbitrado[62,2], ". O valor máximo e minímo foram: (",DB_ASIS_Completo_Arbitrado[62,3], ",", DB_ASIS_Completo_Arbitrado[62,4],")")} else {"Evento não arbitrado"}
    ) 
    
    row.names(eventos_pdf_arb) <- c("Nro Afast. > 15d - Doença Ocup.:",
                                    "Nro Afast. > 15d - Acidente Típico:",
                                    "Nro Afast. < 15d - Doença Ocupacional:",
                                    "Nro Afast. < 15d - Acidente Típico:",
                                    "Taxa Óbitos - Acidente Típico (óbitos/ano):",
                                    "Nro Total de Dias em Falta sem Atestado:",
                                    "Taxa Multa 1 (multas 1/ano)",
                                    "Taxa Multa 2 (multas 2/ano)",
                                    "Taxa Multa 3 (multas 3/ano)",
                                    "Taxa Multa 4 (multas 4/ano)",
                                    "Taxa Multa 5 (multas 5/ano)",
                                    "Taxa Interdição (interdição/ano)"
    )

    
    #### 1.5 Cálculo das Variáveis ####    
    
    ### Variáveis INSS
    #+++++++++++++++++++++++++++++++++++++++++++++
    # 1. Tratar dados INSS e popular no BD_Empresa_Observado
    # a. Benefícios únicos (remove os benefícios duplicados)
    dataset_empresa_INSS_df_unique = distinct(dataset_ASIS_Simple_INSS, Número.do.Benefício, .keep_all = TRUE)
    # b. Converte data em ano
    dataset_empresa_INSS_df_unique$Data.de.Despacho.do.Benefício..DDB. = lubridate::year(dataset_empresa_INSS_df_unique$Data.de.Despacho.do.Benefício..DDB.)
    
    dataset_empresa_INSS_df_unique_B91 = dataset_empresa_INSS_df_unique[grep("B91", dataset_empresa_INSS_df_unique$Espécie.de.Benefício), ]
    dataset_empresa_INSS_df_unique_B92 = dataset_empresa_INSS_df_unique[grep("B92", dataset_empresa_INSS_df_unique$Espécie.de.Benefício), ]
    dataset_empresa_INSS_df_unique_B93 = dataset_empresa_INSS_df_unique[grep("B93", dataset_empresa_INSS_df_unique$Espécie.de.Benefício), ]
    dataset_empresa_INSS_df_unique_B94 = dataset_empresa_INSS_df_unique[grep("B94", dataset_empresa_INSS_df_unique$Espécie.de.Benefício), ]
    
    # c. Consolidação dos dados (Nro total de benefícios, Valor total pago)
    NRO_B91_INSS = table(dataset_empresa_INSS_df_unique_B91$Data.de.Despacho.do.Benefício..DDB., dataset_empresa_INSS_df_unique_B91$Espécie.de.Benefício)
    if(!is.na(NRO_B91_INSS[1])){colnames(NRO_B91_INSS) <- c("NB_91")} 
    NRO_B92_INSS = table(dataset_empresa_INSS_df_unique_B92$Data.de.Despacho.do.Benefício..DDB., dataset_empresa_INSS_df_unique_B92$Espécie.de.Benefício)
    if(!is.na(NRO_B92_INSS[1])){colnames(NRO_B92_INSS) <- c("NB_92")}
    NRO_B93_INSS = table(dataset_empresa_INSS_df_unique_B93$Data.de.Despacho.do.Benefício..DDB., dataset_empresa_INSS_df_unique_B93$Espécie.de.Benefício)
    if(!is.na(NRO_B93_INSS[1])){colnames(NRO_B93_INSS) <- c("NB_93")}
    NRO_B94_INSS = table(dataset_empresa_INSS_df_unique_B94$Data.de.Despacho.do.Benefício..DDB., dataset_empresa_INSS_df_unique_B94$Espécie.de.Benefício)
    if(!is.na(NRO_B94_INSS[1])){colnames(NRO_B94_INSS) <- c("NB_94")}

    Valor_B91_INSS = summarise(group_by(dataset_empresa_INSS_df_unique_B91, Data.de.Despacho.do.Benefício..DDB., Espécie.de.Benefício), sum(Total.Pago.Projeção..R..))
    colnames(Valor_B91_INSS) <- c("anos_bd", "NB", "Aux_DespesaTotalB91" )
    Valor_B91_INSS$NB <- NULL
    Valor_B92_INSS = summarise(group_by(dataset_empresa_INSS_df_unique_B92, Data.de.Despacho.do.Benefício..DDB., Espécie.de.Benefício), sum(Total.Pago.Projeção..R..))
    colnames(Valor_B92_INSS) <- c("anos_bd", "NB", "Aux_DespesaTotalB92" )
    Valor_B92_INSS$NB <- NULL
    Valor_B93_INSS = summarise(group_by(dataset_empresa_INSS_df_unique_B93, Data.de.Despacho.do.Benefício..DDB., Espécie.de.Benefício), sum(Total.Pago.Projeção..R..))
    colnames(Valor_B93_INSS) <- c("anos_bd", "NB", "Aux_DespesaTotalB93" )
    Valor_B93_INSS$NB <- NULL
    Valor_B94_INSS = summarise(group_by(dataset_empresa_INSS_df_unique_B94, Data.de.Despacho.do.Benefício..DDB., Espécie.de.Benefício), sum(Total.Pago.Projeção..R..))
    colnames(Valor_B94_INSS) <- c("anos_bd", "NB", "Aux_DespesaTotalB94" )
    Valor_B94_INSS$NB <- NULL
    
    # Adiciona as variáveis B9x e Custo B9x ao BD
    anos_bd = as.numeric(row.names(NRO_B91_INSS))
    NRO_B91_INSS = cbind(anos_bd, NRO_B91_INSS)
    anos_bd = as.numeric(row.names(NRO_B92_INSS))
    NRO_B92_INSS = cbind(anos_bd, NRO_B92_INSS)
    anos_bd = as.numeric(row.names(NRO_B93_INSS))
    NRO_B93_INSS = cbind(anos_bd, NRO_B93_INSS)
    anos_bd = as.numeric(row.names(NRO_B94_INSS))
    NRO_B94_INSS = cbind(anos_bd, NRO_B94_INSS)
    
    # Agrega as variáveis NB91,92,93 e 94 e Despesa Total NB91,92,93 e 94 ao banco de dados
    DB_ASIS_Simple_Outros_Observado_t = merge(DB_ASIS_Simple_Outros_Observado_t, NRO_B91_INSS, by = "anos_bd", all.x = TRUE)
    if(is.null(DB_ASIS_Simple_Outros_Observado_t$NB_91)){DB_ASIS_Simple_Outros_Observado_t$NB_91 = rep(NA, 10)}
    DB_ASIS_Simple_Outros_Observado_t = merge(DB_ASIS_Simple_Outros_Observado_t, NRO_B92_INSS, by = "anos_bd", all.x = TRUE)
    if(is.null(DB_ASIS_Simple_Outros_Observado_t$NB_92)){DB_ASIS_Simple_Outros_Observado_t$NB_92 = rep(NA, 10)}
    DB_ASIS_Simple_Outros_Observado_t = merge(DB_ASIS_Simple_Outros_Observado_t, NRO_B93_INSS, by = "anos_bd", all.x = TRUE)
    if(is.null(DB_ASIS_Simple_Outros_Observado_t$NB_93)){DB_ASIS_Simple_Outros_Observado_t$NB_93 = rep(NA, 10)}
    DB_ASIS_Simple_Outros_Observado_t = merge(DB_ASIS_Simple_Outros_Observado_t, NRO_B94_INSS, by = "anos_bd", all.x = TRUE)
    if(is.null(DB_ASIS_Simple_Outros_Observado_t$NB_94)){DB_ASIS_Simple_Outros_Observado_t$NB_94 = rep(NA, 10)}
    
    DB_ASIS_Simple_Outros_Observado_t = merge(DB_ASIS_Simple_Outros_Observado_t, Valor_B91_INSS, by = "anos_bd", all.x = TRUE)
    DB_ASIS_Simple_Outros_Observado_t = merge(DB_ASIS_Simple_Outros_Observado_t, Valor_B92_INSS, by = "anos_bd", all.x = TRUE)
    DB_ASIS_Simple_Outros_Observado_t = merge(DB_ASIS_Simple_Outros_Observado_t, Valor_B93_INSS, by = "anos_bd", all.x = TRUE)
    DB_ASIS_Simple_Outros_Observado_t = merge(DB_ASIS_Simple_Outros_Observado_t, Valor_B94_INSS, by = "anos_bd", all.x = TRUE)

    ## Criar o objeto DB_Calc com as variáveis obervadas (DB_Calc banco de dados que apresenta em cada coluna uma variável preenchida pelo usuário e calculada pelo sistema)
    DB_Calc = DB_ASIS_Simple_Outros_Observado_t
    
    # <Revisão Kepler Weber - Mai/2018>
    # Os números de NBs observados nos dados do INSS deve receber valor igual a zero quando de fato não houve evento no período analisado.
    # Ano mais antigo e mais novo dos eventos obsercados no INSS
    INSS_Ano_Min = min(NRO_B91_INSS[,1], NRO_B92_INSS[,1], NRO_B93_INSS[,1], NRO_B94_INSS[,1])
    INSS_Ano_Max = max(NRO_B91_INSS[,1], NRO_B92_INSS[,1], NRO_B93_INSS[,1], NRO_B94_INSS[,1])
    # Seleciona as variáveis calculadas do INSS NBs e Custo Total NBs e os anos min e max
    DB_INSS_Ajuste_NA = DB_Calc %>% 
      select(anos_bd, NB_91,	NB_92,	NB_93,	NB_94,	Aux_DespesaTotalB91,	Aux_DespesaTotalB92,	Aux_DespesaTotalB93,	Aux_DespesaTotalB94) %>%
      dplyr::filter(anos_bd >= INSS_Ano_Min & anos_bd <= INSS_Ano_Max)
    # Substitui NA por zero
    DB_INSS_Ajuste_NA[is.na(DB_INSS_Ajuste_NA)] <- 0
    # Remove as variáveis originais NBs e Custo Total INSS
    DB_Calc$NB_91 <- NULL
    DB_Calc$NB_92 <- NULL
    DB_Calc$NB_93 <- NULL
    DB_Calc$NB_94 <- NULL
    DB_Calc$Aux_DespesaTotalB91 <- NULL
    DB_Calc$Aux_DespesaTotalB92 <- NULL
    DB_Calc$Aux_DespesaTotalB93 <- NULL
    DB_Calc$Aux_DespesaTotalB94 <- NULL
    # Substitui no DB_Calc os dados INSS sem NA
    DB_Calc = merge(DB_Calc, DB_INSS_Ajuste_NA, by = "anos_bd", all.x = TRUE, all.y=TRUE)

    ## Variaveis auxiliares
    Nro_AfMenor15 = DB_Calc$Nev_Afmenor15_DoenOcup + DB_Calc$Nev_Afmenor15_NRelac + DB_Calc$Nev_Afmenor15_Tipico + DB_Calc$Nev_Afmenor15_Trajeto
    Nro_AfMaior15 = DB_Calc$Nev_Afmaior15_DoenOcup + DB_Calc$Nev_Afmaior15_NRelac + DB_Calc$Nev_Afmaior15_Tipico + DB_Calc$Nev_Afmaior15_Trajeto
    Nro_AfMaior15_Doenca_Acidente = DB_Calc$Nev_Afmaior15_DoenOcup + DB_Calc$Nev_Afmaior15_Tipico
    Nro_AfMaior15_Doenca_Acidente_trajeto = DB_Calc$Nev_Afmaior15_DoenOcup + DB_Calc$Nev_Afmaior15_Tipico + DB_Calc$Nev_Afmaior15_Trajeto
    Obitos_Tipico_trajeto = DB_Calc$Nev_Obito_Tipico + DB_Calc$Nev_Obito_Trajeto
    Nro_AEventos_Doenca_Acidente = DB_Calc$Nev_Afmaior15_DoenOcup + DB_Calc$Nev_Afmaior15_Tipico + DB_Calc$Nev_Afmenor15_DoenOcup + DB_Calc$Nev_Afmenor15_Tipico + DB_Calc$Nev_Safast_DoenOcup + DB_Calc$Nev_Safast_Tipico
    Nro_AEventos_Acidente = DB_Calc$Nev_Afmaior15_Tipico + DB_Calc$Nev_Afmenor15_Tipico + DB_Calc$Nev_Safast_Tipico
    Nro_Eventos_Tipicos = DB_Calc$Nev_Afmaior15_Tipico + DB_Calc$Nev_Afmenor15_Tipico + DB_Calc$Nev_Safast_Tipico + DB_Calc$Nev_Obito_Tipico
    Nro_Eventos_Ocup_excetosemafast = DB_Calc$Nev_Afmaior15_DoenOcup + DB_Calc$Nev_Afmenor15_DoenOcup + DB_Calc$Nev_Obito_DoenOcup
    
    # Variaveis da calculadora Básico
    NB_91_Inicial = sum(DB_Calc$NB_91, na.rm = TRUE)
    NB_92_Inicial = sum(DB_Calc$NB_92, na.rm = TRUE)
    NB_93_Inicial = sum(DB_Calc$NB_93, na.rm = TRUE)
    NB_94_Inicial = sum(DB_Calc$NB_94, na.rm = TRUE)
    Soma_NBs = NB_91_Inicial + NB_92_Inicial + NB_93_Inicial + NB_94_Inicial
    if(sum(DB_Calc$Aux_DespesaTotalB91, na.rm = TRUE) > 0){CustoMedio_NB_91 = (DB_Calc$Aux_DespesaTotalB91 / DB_Calc$NB_91)} else {CustoMedio_NB_91 = rep(0, 10)}
    if(sum(DB_Calc$Aux_DespesaTotalB92, na.rm = TRUE) > 0){CustoMedio_NB_92 = (DB_Calc$Aux_DespesaTotalB92 / DB_Calc$NB_92)} else {CustoMedio_NB_92 = rep(0, 10)}
    if(sum(DB_Calc$Aux_DespesaTotalB93, na.rm = TRUE) > 0){CustoMedio_NB_93 = (DB_Calc$Aux_DespesaTotalB93 / DB_Calc$NB_93)} else {CustoMedio_NB_93 = rep(178000, 10)}
    if(sum(DB_Calc$Aux_DespesaTotalB94, na.rm = TRUE) > 0){CustoMedio_NB_94 = (DB_Calc$Aux_DespesaTotalB94 / DB_Calc$NB_94)} else {CustoMedio_NB_94 = rep(0, 10)}
    DesligamentosVoluntarios = DB_Calc$Aux_NroTotalDesligamentos - DB_Calc$DesligamentosInvoluntarios
    PercDesligamentosVoluntarios = DesligamentosVoluntarios / DB_Calc$Funcionarios
    DiasMedAfast_Men15 = DB_Calc$Aux_TotalDiasAfast_Men15 / Nro_AfMenor15
    HorasPorDia = DB_Calc$Aux_TotalHorasTrabalhadas / (DB_Calc$Funcionarios * DB_Calc$DiasUteis)
    Pev_Afmenor15_Tipico    = DB_Calc$Nev_Afmenor15_Tipico / DB_Calc$Funcionarios
    Pev_Afmaior15_Tipico    = DB_Calc$Nev_Afmaior15_Tipico / DB_Calc$Funcionarios
    Pev_Safast_Tipico       = DB_Calc$Nev_Safast_Tipico / DB_Calc$Funcionarios
    Pev_Obito_Tipico        = DB_Calc$Nev_Obito_Tipico / DB_Calc$Funcionarios
    Pev_Afmenor15_Trajeto   = DB_Calc$Nev_Afmenor15_Trajeto / DB_Calc$Funcionarios
    Pev_Afmaior15_Trajeto   = DB_Calc$Nev_Afmaior15_Trajeto / DB_Calc$Funcionarios
    Pev_Safast_Trajeto      = DB_Calc$Nev_Safast_Trajeto / DB_Calc$Funcionarios
    Pev_Obito_Trajeto       = DB_Calc$Nev_Obito_Trajeto / DB_Calc$Funcionarios
    Pev_Afmenor15_DoenOcup  = DB_Calc$Nev_Afmenor15_DoenOcup / DB_Calc$Funcionarios
    Pev_Afmaior15_DoenOcup  = DB_Calc$Nev_Afmaior15_DoenOcup / DB_Calc$Funcionarios
    Pev_Safast_DoenOcup     = DB_Calc$Nev_Safast_DoenOcup / DB_Calc$Funcionarios
    Pev_Obito_DoenOcup      = DB_Calc$Nev_Obito_DoenOcup / DB_Calc$Funcionarios
    Pev_Afmenor15_NRelac    = DB_Calc$Nev_Afmenor15_NRelac / DB_Calc$Funcionarios
    Pev_Afmaior15_NRelac    = DB_Calc$Nev_Afmaior15_NRelac / DB_Calc$Funcionarios
    Pev_Safast_NRelac       = DB_Calc$Nev_Safast_NRelac / DB_Calc$Funcionarios
    Pev_Obito_NRelac        = DB_Calc$Nev_Obito_NRelac / DB_Calc$Funcionarios
    TaxaFaltas = DB_Calc$Aux_NroTotalDias_Faltas / DB_Calc$Funcionarios
    
    # Garante o emparelhamento dos dados para calculo dos fatores
    DF_PInval = cbind(DB_Calc$NB_92, Nro_AfMaior15)
    DF_PInval = data.frame(na.omit(DF_PInval))
    
    # <Revisão Kepler Weber - Mai/2018>
    # Há situações em que o evento B92 ocorreu em 2012 e a série de dados sobre afastamento (> 15 dias) retorna até 2014. Por consequencia, o modelo matemático 
    # não consegue correlacionar os eventos do INSS ao parâmetros sobre afastamento informados para o "Cálculo da Probabilidade de Invalidez". Assim sendo, 
    # i) alterar o código da calculadora para realizar o cálculo da probabilidade de 
    # invalidez utilizando os eventos de afastamento > 15 dias informados pelo INSS (Dados informados na Aba Simplificado_INSS).
    # Se existe ocorrência NB92 então calcula a PInvaliz a partir dos dados do INSS, senão PInvalidez é zero
    if(sum(DB_Calc$NB_92, na.rm = TRUE) > 0){
      PInvalidez = sum(DB_Calc$NB_92, na.rm = TRUE) / (sum(DB_Calc$NB_91, na.rm = TRUE) + sum(DB_Calc$NB_92, na.rm = TRUE) + sum(DB_Calc$NB_93, na.rm = TRUE) + sum(DB_Calc$NB_94, na.rm = TRUE))
    } else {PInvalidez = rep(0, 10)}
    
    # <Revisão Kepler Weber - Mai/2018>
    # Os dados do INSS e dos números de Eventos informados pelo usuário devem ser alinhados temporalmente para o cálculo dos fatoresB91,92,93 e 94.
    DF_NB91 = cbind(DB_Calc$NB_91, Nro_AfMaior15_Doenca_Acidente_trajeto)
    DF_NB91 = data.frame(na.omit(DF_NB91))
    if((sum(DF_NB91[,1], na.rm = TRUE) / sum(DF_NB91[,2], na.rm = TRUE)) > 0) {FatorB91 = sum(DF_NB91[,1], na.rm = TRUE) / sum(DF_NB91[,2], na.rm = TRUE)} else {FatorB91 = rep(0, 10)}

    # Revisão Kepler Weber: Acidentes de Trajeto devem ser incoporados ao denominador, pois o INSS classifica B91, B92, B93, e B94 dos acidentes de trajeto.
    DF_NB92 = cbind(DB_Calc$NB_92, Nro_AfMaior15_Doenca_Acidente_trajeto)
    DF_NB92[is.na(DF_NB92)] <- 0
    DF_NB92 = data.frame(DF_NB92[4:8,])
    if((sum(DF_NB92[,1], na.rm = TRUE) / sum(DF_NB92[,2], na.rm = TRUE)) > 0) {FatorB92 = sum(DF_NB92[,1], na.rm = TRUE) / sum(DF_NB92[,2], na.rm = TRUE)} else {FatorB92 = rep(0, 10)}
    
    DF_NB93 = cbind(DB_Calc$NB_93, Obitos_Tipico_trajeto)
    DF_NB93[is.na(DF_NB93)] <- 0
    DF_NB93 = data.frame(DF_NB93[4:8,])
    if(sum(DB_Calc$NB_93, na.rm = TRUE) > 0) {FatorB93 = sum(DF_NB93[,1], na.rm = TRUE) / sum(DF_NB93[,2], na.rm = TRUE)} else {FatorB93 = rep(1, 10)}
    FatorB93 = rep(1, 10)
    
    DF_NB94 = cbind(DB_Calc$NB_94, Nro_AfMaior15_Doenca_Acidente_trajeto)
    DF_NB94[is.na(DF_NB94)] <- 0
    DF_NB94 = data.frame(DF_NB94[4:8,])
    if(length(DF_NB94[,1]) > 0) {FatorB94 = sum(DF_NB94[,1], na.rm = TRUE) / sum(DF_NB94[,2], na.rm = TRUE)} else {FatorB94 = rep(0, 10)}
    
    TempoComputadoMedio = DB_Calc$TaxaGravidade / DB_Calc$TaxaFrequencia
    
    # Fatores de ajustes para associar eventos observados com as taxas de frequencia e taxa de gracidade
    FatorAjusteExposicaoAoRisco = DB_Calc$HorasHomemExposicaoRisco / DB_Calc$Aux_TotalHorasTrabalhadas
    EventosTFTG = (DB_Calc$TaxaFrequencia[10] * DB_Calc$HorasHomemExposicaoRisco[10]) / 1000000
    FatorEventosTFTG = EventosTFTG / (Nro_Eventos_Tipicos + Nro_Eventos_Ocup_excetosemafast)
    
    var_basico= cbind(NB_91_Inicial,
                      NB_92_Inicial,
                      NB_93_Inicial,
                      NB_94_Inicial,
                      Soma_NBs,
                      CustoMedio_NB_91,
                      CustoMedio_NB_92,
                      CustoMedio_NB_93,
                      CustoMedio_NB_94,
                      DesligamentosVoluntarios,
                      PercDesligamentosVoluntarios,
                      DiasMedAfast_Men15,
                      HorasPorDia,
                      Pev_Afmenor15_Tipico,
                      Pev_Afmaior15_Tipico,
                      Pev_Safast_Tipico,
                      Pev_Obito_Tipico,
                      Pev_Afmenor15_Trajeto,
                      Pev_Afmaior15_Trajeto,
                      Pev_Safast_Trajeto,
                      Pev_Obito_Trajeto,
                      Pev_Afmenor15_DoenOcup,
                      Pev_Afmaior15_DoenOcup,
                      Pev_Safast_DoenOcup,
                      Pev_Obito_DoenOcup,
                      Pev_Afmenor15_NRelac,
                      Pev_Afmaior15_NRelac,
                      Pev_Safast_NRelac,
                      Pev_Obito_NRelac,
                      TaxaFaltas,
                      PInvalidez,
                      FatorB91,
                      FatorB92,
                      FatorB93,
                      FatorB94,
                      TempoComputadoMedio,
                      FatorAjusteExposicaoAoRisco,
                      FatorEventosTFTG)
    
    DB_Calc = cbind(DB_Calc, var_basico)
    
    # Variaveis da calculadora Custom_DespesasEvitáveis
    # <Revisão Kepler Weber - Mai/2018>
    # O Número total de funcionário desligados deve ser pareado com a Probabilidade do Funcionário Ajuizar e Ganhar Ação Trabalhista (PReclamatoria). Caso não tenhas sido utilizada a variável PReclamatoria somar o nro dos últimos 3 anos
    DF_TotalFuncionarios = cbind(DB_Calc$Aux_NroTotalDesligamentos, DB_Calc$PReclamatoria)
    DF_TotalFuncionarios = data.frame(na.omit(DF_TotalFuncionarios))
    if(is.na(DF_TotalFuncionarios[1,1])){FuncionariosDesligados = sum(DB_Calc[8:10,5], na.rm = TRUE)} else {FuncionariosDesligados = sum(DF_TotalFuncionarios[,1])}

    PAcaoRegressiva = sum(DB_Calc$Aux_NroAcoesRegre, na.rm = TRUE) / Soma_NBs
    DespesaMedicaMedia = DB_Calc$Aux_DespesaMedicaTotal / Nro_AEventos_Doenca_Acidente
    
    ReajustePlanoEstimado = rep(NA, length(DB_Calc$DespesasPlanoInicial))
    for(k in 1: length(DB_Calc$DespesasPlanoInicial)){
      if(k>1){
        ReajustePlanoEstimado[k] = (DB_Calc$DespesasPlanoInicial[k] - DB_Calc$DespesasPlanoInicial[k-1])/DB_Calc$DespesasPlanoInicial[k-1]
      }
    }
    
    Aux_DiasMediosInterup_Obitos = DB_Calc$DiasInterrupcaoAcidenteObito / DB_Calc$Nev_Obito_Tipico
    Aux_DiasMediosInterup_Acidentes = DB_Calc$DiasInterrupcaoAcidenteOutros / Nro_AEventos_Acidente
    if(sum(DB_Calc$Aux_LucroCessanteTotal_Obitos[9:10] / DB_Calc$DiasInterrupcaoAcidenteObito[9:10] , na.rm = TRUE) > 0) {
      LucroCessanteAcidenteObito = DB_Calc$Aux_LucroCessanteTotal_Obitos / DB_Calc$DiasInterrupcaoAcidenteObito} else {LucroCessanteAcidenteObito = rep(0, 10)}
    if(sum(DB_Calc$Aux_LucroCessanteTotal_OutrosAcid / DB_Calc$DiasInterrupcaoAcidenteOutros, na.rm = TRUE) > 0) {
      LucroCessanteAcidenteOutros = DB_Calc$Aux_LucroCessanteTotal_OutrosAcid / DB_Calc$DiasInterrupcaoAcidenteOutros} else {LucroCessanteAcidenteOutros = rep(0, 10)}
    
    PercentualReabilitacao = DB_Calc$Aux_NroTotalReabilitados / Nro_AEventos_Doenca_Acidente
    
    var_Custom_DespesasEvitaveis= cbind(FuncionariosDesligados,
                                        PAcaoRegressiva,
                                        DespesaMedicaMedia,
                                        ReajustePlanoEstimado,
                                        Aux_DiasMediosInterup_Obitos,
                                        Aux_DiasMediosInterup_Acidentes,
                                        LucroCessanteAcidenteObito,
                                        LucroCessanteAcidenteOutros,
                                        PercentualReabilitacao)
    
    
    DB_Calc = cbind(DB_Calc, var_Custom_DespesasEvitaveis)
    
    # Variaveis da calculadora Custom_MelhorUsoRecursos
    CustoMedioMPInsumos = DB_Calc$Aux_DespTotal_MPeInsumos / Nro_AEventos_Doenca_Acidente
    CustoMedioRefugoRetrabalho = DB_Calc$Aux_DespTotal_RefugoeRetrabalho / Nro_AEventos_Doenca_Acidente
    
    var_Custom_Custom_MelhorUsoRecursos= cbind(CustoMedioMPInsumos,
                                               CustoMedioRefugoRetrabalho)
    DB_Calc = cbind(DB_Calc, var_Custom_Custom_MelhorUsoRecursos)
    
    # Variaveis da calculadora Intagível
    CustoMedSubstituporTempo = DB_Calc$Aux_CustoContratacaoUmFuncionario / DB_Calc$TempoContratacaoPadrao 
    DB_Calc = cbind(DB_Calc, CustoMedSubstituporTempo)
    
    # Regressao Desligamentos voluntários
    # Garante o emparelhamento dos dados para calculo dos fatores
    DF_RegDesl = cbind(DB_Calc$PercDesligamentosVoluntarios, DB_Calc$VarPIB, DB_Calc$TaxaGravidade)
    DF_RegDesl = data.frame(na.omit(DF_RegDesl)); colnames(DF_RegDesl) <- c("DesligamentosVoluntarios", "VarPIB", "TaxaGravidade")
    # Caso não exista paridade de três pontos de dados para as variáveis da regressão não rodar a regressão
    if(length(DF_RegDesl[,1]) >= 3){
      if(length(DF_RegDesl[,1]) > 4){
        regressao_DesligamentosVoluntarios = lm(DesligamentosVoluntarios ~ VarPIB + TaxaGravidade, DF_RegDesl)
        if(regressao_DesligamentosVoluntarios$coefficients[1] > -1000) {Beta0DesligVoluntarios = regressao_DesligamentosVoluntarios$coefficients[1]} else {Beta0DesligVoluntarios = rep(-99.99, 10)}
        if(regressao_DesligamentosVoluntarios$coefficients[2] > -1000) {BetaPIBDesigVoluntarios = regressao_DesligamentosVoluntarios$coefficients[2]} else {BetaPIBDesigVoluntarios = rep(-99.99, 10)}
        # <Revisão do modelo: Variável Taxa de Frequencia eliminada da função)>
        BetaFreqDesligVoluntarios = rep(0, 10)
        if(regressao_DesligamentosVoluntarios$coefficients[3] > -1000) {BetaGravDesligVoluntarios = regressao_DesligamentosVoluntarios$coefficients[3]} else {BetaGravDesligVoluntarios = rep(-99.99, 10)}
      } else {
        regressao_DesligamentosVoluntarios = lm(DesligamentosVoluntarios ~ TaxaGravidade, DF_RegDesl)
        if(regressao_DesligamentosVoluntarios$coefficients[1] > -1000) {Beta0DesligVoluntarios = regressao_DesligamentosVoluntarios$coefficients[1]} else {Beta0DesligVoluntarios = rep(-99.99, 10)}
        # Neste caso o PIB é desconsiderado do modelo
        BetaPIBDesigVoluntarios = rep(0, 10)
        # <Revisão do modelo: Variável Taxa de Frequencia eliminada da função)>
        BetaFreqDesligVoluntarios = rep(0, 10)
        if(regressao_DesligamentosVoluntarios$coefficients[2] > -1000) {BetaGravDesligVoluntarios = regressao_DesligamentosVoluntarios$coefficients[2]} else {BetaGravDesligVoluntarios = rep(-99.99, 10)}
      }
    } else {Beta0DesligVoluntarios = rep(-99.99, 10); BetaPIBDesigVoluntarios = rep(-99.99, 10); BetaGravDesligVoluntarios = rep(-99.99, 10); BetaFreqDesligVoluntarios = rep(-99.99, 10)}
    
    # Regressao Percentilcusto FAP
    DF_FAPCusto = cbind(DB_Calc$Percentilcusto, DB_Calc$Indicecusto)
    DF_FAPCusto = data.frame(na.omit(DF_FAPCusto)); colnames(DF_FAPCusto) <- c("Percentilcusto", "Indicecusto")
    if(length(DF_FAPCusto[,1]) >= 3){
      regressao_PercentilcustoFAP = lm(Percentilcusto ~ Indicecusto, DF_FAPCusto)
      if(!is.na(regressao_PercentilcustoFAP$coefficients[1]) & regressao_PercentilcustoFAP$coefficients[1] > -1000){Beta0ICustoFAP = regressao_PercentilcustoFAP$coefficients[1]} else {Beta0ICustoFAP = rep(-99.99, 10)}
      if(!is.na(regressao_PercentilcustoFAP$coefficients[2]) & regressao_PercentilcustoFAP$coefficients[2] > -1000){Beta1ICustoFAP = regressao_PercentilcustoFAP$coefficients[2]} else {Beta1ICustoFAP = rep(-99.99, 10)}
    } else {Beta0ICustoFAP = rep(-99.99, 10); Beta1ICustoFAP = rep(-99.99, 10)}
    
    # Regressao Percentilfrequencia FAP
    DF_FAPFreq = cbind(DB_Calc$Percentilfrequencia, DB_Calc$Indicefrequencia)
    DF_FAPFreq = data.frame(na.omit(DF_FAPFreq)); colnames(DF_FAPFreq) <- c("Percentilfrequencia", "Indicefrequencia")
    if(length(DF_FAPFreq[,1]) >= 3){
      regressao_PercentilfrequenciaFAP = lm(Percentilfrequencia ~ Indicefrequencia, DF_FAPFreq)
      if(!is.na(regressao_PercentilfrequenciaFAP$coefficients[1]) & regressao_PercentilfrequenciaFAP$coefficients[1] > -1000){Beta0IFrequenciaFAP = regressao_PercentilfrequenciaFAP$coefficients[1]} else {Beta0IFrequenciaFAP = rep(-99.99, 10)}
      if(!is.na(regressao_PercentilfrequenciaFAP$coefficients[2]) & regressao_PercentilfrequenciaFAP$coefficients[2] > -1000){Beta1IFrequenciaFAP = regressao_PercentilfrequenciaFAP$coefficients[2]} else {Beta1IFrequenciaFAP = rep(-99.99, 10)}
    } else {Beta0IFrequenciaFAP = rep(-99.99, 10); Beta1IFrequenciaFAP = rep(-99.99, 10)}
    
    # Regressao Percentilgravidade FAP
    DF_FAPGrav = cbind(DB_Calc$Percentilgravidade, DB_Calc$Indicegravidade)
    DF_FAPGrav = data.frame(na.omit(DF_FAPGrav)); colnames(DF_FAPGrav) <- c("Percentilgravidade", "Indicegravidade")
    if(length(DF_FAPGrav[,1]) >= 3){
      regressao_PercentilgravidadeFAP = lm(Percentilgravidade ~ Indicegravidade, DF_FAPGrav)
      if(!is.na(regressao_PercentilgravidadeFAP$coefficients[1]) & regressao_PercentilgravidadeFAP$coefficients[1] > -1000) {Beta0IGravidadeFAP = regressao_PercentilgravidadeFAP$coefficients[1]} else {Beta0IGravidadeFAP = rep(-99.99, 10)}
      if(!is.na(regressao_PercentilgravidadeFAP$coefficients[2]) & regressao_PercentilgravidadeFAP$coefficients[2] > -1000) {Beta1IGravidadeFAP = regressao_PercentilgravidadeFAP$coefficients[2]} else {Beta1IGravidadeFAP = rep(-99.99, 10)}
    } else {Beta0IGravidadeFAP = rep(-99.99, 10); Beta1IGravidadeFAP = rep(-99.99, 10)}
    
    # Regressao ReajustePlanoP
    DF_ReajPlan = cbind(DB_Calc$ReajustePlanoEstimado, DB_Calc$TaxaGravidade)
    DF_ReajPlan = data.frame(na.omit(DF_ReajPlan)); colnames(DF_ReajPlan) <- c("ReajustePlanoEstimado", "TaxaGravidade")
    if(length(DF_ReajPlan[,1]) >= 3){
      regressao_ReajustePlano = lm(ReajustePlanoEstimado ~ TaxaGravidade, DF_ReajPlan)
      if(!is.na(regressao_ReajustePlano$coefficients[1]) & regressao_ReajustePlano$coefficients[1] > -1000) {Beta0ReajustePlano = regressao_ReajustePlano$coefficients[1]} else {Beta0ReajustePlano = rep(-99.99, 10)}
      # <Revisão do modelo: Variável Taxa de Frequencia eliminada da função)>
      BetaFreqReajustePlano = rep(0, 10)
      if(!is.na(regressao_ReajustePlano$coefficients[2]) & regressao_ReajustePlano$coefficients[2] > -1000) {BetaGravReajustePlano = regressao_ReajustePlano$coefficients[2]} else {BetaGravReajustePlano = rep(-99.99, 10)}
    } else {Beta0ReajustePlano = rep(-99.99, 10); BetaGravReajustePlano = rep(-99.99, 10); BetaFreqReajustePlano = rep(-99.99, 10)}
    
    # Regressao Tempo Contratação
    DF_TempoCont = cbind(DB_Calc$TempoContratacaoPadrao, DB_Calc$TaxaGravidade, DB_Calc$VarPIB)
    DF_TempoCont = data.frame(na.omit(DF_TempoCont)); colnames(DF_TempoCont) <- c("TempoContratacaoPadrao", "TaxaGravidade", "VarPIB")
    if(length(DF_TempoCont[,1]) >= 3){    
      if(length(DF_TempoCont[,1]) > 4){
        regressao_ImagemTempoCont = lm(TempoContratacaoPadrao ~ TaxaGravidade + VarPIB, DF_TempoCont)
        if(regressao_ImagemTempoCont$coefficients[1] > -1000) {Beta0TempoContratacao = regressao_ImagemTempoCont$coefficients[1]} else {Beta0TempoContratacao = rep(-99.99, 10)}
        # <Revisão do modelo: Variável Taxa de Frequencia eliminada da função)>
        BetaFreqTempoContratacao = rep(0, 10)
        if(regressao_ImagemTempoCont$coefficients[2] > -1000) {BetaGravTempoContratacao = regressao_ImagemTempoCont$coefficients[2]} else {BetaGravTempoContratacao = rep(-99.99, 10)}
        if(regressao_ImagemTempoCont$coefficients[2] > -1000) {BetaPIBTempoContratacao = regressao_ImagemTempoCont$coefficients[3]} else {BetaPIBTempoContratacao = rep(-99.99, 10)}
      } else {
        regressao_ImagemTempoCont = lm(TempoContratacaoPadrao ~ TaxaGravidade, DF_TempoCont)
        if(regressao_ImagemTempoCont$coefficients[1] > -1000) {Beta0TempoContratacao = regressao_ImagemTempoCont$coefficients[1]} else {Beta0TempoContratacao = rep(-99.99, 10)}
        # <Revisão do modelo: Variável Taxa de Frequencia eliminada da função)>
        BetaFreqTempoContratacao = rep(0, 10)
        if(regressao_ImagemTempoCont$coefficients[2] > -1000) {BetaGravTempoContratacao = regressao_ImagemTempoCont$coefficients[2]} else {BetaGravTempoContratacao = rep(-99.99, 10)}
        BetaPIBTempoContratacao = rep(0, 10)
      }
    } else {Beta0TempoContratacao = rep(-99.99, 10); BetaGravTempoContratacao = rep(-99.99, 10); BetaPIBTempoContratacao = rep(-99.99, 10); BetaFreqTempoContratacao = rep(-99.99, 10)}
    
    var_regressoes= cbind(Beta0DesligVoluntarios, 
                          BetaPIBDesigVoluntarios, 
                          BetaFreqDesligVoluntarios, 
                          BetaGravDesligVoluntarios, 
                          Beta0ICustoFAP, 
                          Beta1ICustoFAP, 
                          Beta0IFrequenciaFAP, 
                          Beta1IFrequenciaFAP, 
                          Beta0IGravidadeFAP, 
                          Beta1IGravidadeFAP, 
                          Beta0ReajustePlano, 
                          BetaFreqReajustePlano, 
                          BetaGravReajustePlano, 
                          Beta0TempoContratacao, 
                          BetaFreqTempoContratacao,
                          BetaGravTempoContratacao, 
                          BetaPIBTempoContratacao)
    
    
    DB_Calc = cbind(DB_Calc, var_regressoes)
    
    ### Calculo das Prob. dos Eventos e outras variáveis a partir dados arbitrados
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ## Calculo do numero médio de funcionários
    Funcionarios_ultimoano = DB_ASIS_Simple_Outros_Observado_nrofunc[1,10]
    Funcionarios_media = rowMeans(DB_ASIS_Simple_Outros_Observado_nrofunc[5:10], na.rm = TRUE)
    Funcionarios_media_arb = rep(Funcionarios_media, 3)
    names(Funcionarios_media_arb) <- c("Usual", "Máximo", "Mínimo")
    
    DB_ASIS_Completo_Arbitrado_prov = DB_ASIS_Completo_Arbitrado
    DB_ASIS_Completo_Arbitrado_prov$VarModelName <- NULL
    DB_ASIS_Completo_Arbitrado_prov_t = data.frame(t(DB_ASIS_Completo_Arbitrado_prov))
    
    ## Calculo das Pev a partir dados arbitrados
    Pev_Afmaior15_DoenOcup = DB_ASIS_Completo_Arbitrado_prov_t$Nev_Afmaior15_DoenOcup / Funcionarios_ultimoano
    names(Pev_Afmaior15_DoenOcup) <- c("Usual", "Máximo", "Mínimo")
    Pev_Afmaior15_Tipico = DB_ASIS_Completo_Arbitrado_prov_t$Nev_Afmaior15_Tipico / Funcionarios_ultimoano  
    names(Pev_Afmaior15_Tipico) <- c("Usual", "Máximo", "Mínimo")
    Pev_Afmenor15_DoenOcup = DB_ASIS_Completo_Arbitrado_prov_t$Nev_Afmenor15_DoenOcup / Funcionarios_ultimoano
    names(Pev_Afmenor15_DoenOcup) <- c("Usual", "Máximo", "Mínimo")
    Pev_Afmenor15_Tipico = DB_ASIS_Completo_Arbitrado_prov_t$Nev_Afmenor15_Tipico / Funcionarios_ultimoano
    names(Pev_Afmenor15_Tipico) <- c("Usual", "Máximo", "Mínimo")
    Pev_Obito_Tipico = DB_ASIS_Completo_Arbitrado_prov_t$Nev_Obito_Tipico
    names(Pev_Obito_Tipico) <- c("Usual", "Máximo", "Mínimo")
    TaxaFaltas = DB_ASIS_Completo_Arbitrado_prov_t$Aux_NroTotalDias_Faltas / Funcionarios_ultimoano
    names(TaxaFaltas) <- c("Usual", "Máximo", "Mínimo")
    
    # Agregando as variáveis calculadas ao banco de dados (adiconado linhas)
    DB_ASIS_Completo_Arbitrado = cbind(Pev_Afmaior15_DoenOcup,
                                       Pev_Afmaior15_Tipico,
                                       Pev_Afmenor15_DoenOcup,
                                       Pev_Afmenor15_Tipico,
                                       Pev_Obito_Tipico,
                                       TaxaFaltas,
                                       DB_ASIS_Completo_Arbitrado_prov_t)
    DB_ASIS_Completo_Arbitrado = data.frame(t(DB_ASIS_Completo_Arbitrado))
    
    # <Revisão Kepler Weber - Mai/2018>
    # Cáclculo da Variável PAcaoRegressiva em caso de arbitragem da variável
    PAcaoRegressiva_arb = (DB_ASIS_Completo_Arbitrado[61,] / Soma_NBs)
    rownames(PAcaoRegressiva_arb) <- c("PAcaoRegressiva")
    # Salva a nova variável ao objeto
    DB_ASIS_Completo_Arbitrado = rbind(DB_ASIS_Completo_Arbitrado, PAcaoRegressiva_arb)
    
    #### 1.6 Calculo das Estatísticas das variáveis coletadas e calculadas (média e desvio padrão utilizado pela calculadora) ####    
    # Remover colunas de apoio
    DB_Calc$anos_bd <- NULL
    
    #Unir DC_Calc com os 
    DB_Calc$anos_bd <- NULL
    DB_stats = stat.desc(DB_Calc)
    DB_Calc_stats = rbind(DB_Calc, DB_stats)
    anos_bd <- c(lubridate::year(Sys.Date())-10,
                 lubridate::year(Sys.Date())-9,
                 lubridate::year(Sys.Date())-8,
                 lubridate::year(Sys.Date())-7,
                 lubridate::year(Sys.Date())-6,
                 lubridate::year(Sys.Date())-5,
                 lubridate::year(Sys.Date())-4,
                 lubridate::year(Sys.Date())-3,
                 lubridate::year(Sys.Date())-2,
                 lubridate::year(Sys.Date())-1)
    a=stat.desc(DB_Calc$CustoMDO)
    row.names(DB_Calc_stats) <- c(anos_bd, names(a))

    #### 1.7 Teste consistência para garantir sanidade dos dados carregados pelo usuário <Criado em: jun/2018> ####    

    ### Log/Check 0 - Versão da planilha de entrada de dados - ASIS <Criado em: jun/2018>
    # Verifica se a versão da planilha é a mesma versão da planilha vigente
    versao_planilha_asis_vigente = "v1.0"
    
    # Testa se o objeto existe (campo esta preenchido na planilha)
    if(exists("versao_planilha_asis") & length(versao_planilha_asis)){
      # Testa a versão da planilha é a última
    if(versao_planilha_asis == versao_planilha_asis_vigente)
      {AsIS_Logs = matrix(c("0", "Planilha", "Consistência versão planilha", "Ok", "Versão vigente"), nrow=1, ncol=5)} else 
      {AsIS_Logs = matrix(c("0", "Planilha", "Consistência versão planilha", "Erro", "Atualizar os dados para versão vigente (download no menu ao lado)"), nrow=1, ncol=5)}} else {AsIS_Logs = matrix(c("0", "Planilha", "Consistência versão planilha", "Erro", "Atualizar os dados para versão vigente (download no menu ao lado)"), nrow=1, ncol=5)}
    colnames(AsIS_Logs) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")   

    ### Log/Check 01 - Preenchimento Planilha Parametrização ASIS <Criado em: jun/2018>
    # Verifica se a versao da calculadora foi corretamente selecionada pelo usuário
    if(versao_calculadora == "Básica" & sum(dataset_ASIS_param_Modulos) == 0) 
    {AsIS_Logs1 = matrix(c("1", "Parametrização", "Consistência versão calculadora", "Ok", "Módulos selecionados estão ok"), nrow=1, ncol=5)} else 
    {AsIS_Logs1 = matrix(c("1", "Parametrização", "Consistência versão calculadora", "Erro", "Módulos selecionados com problema. Revisar"), nrow=1, ncol=5)}
    if(versao_calculadora == "Simplificada" & dataset_ASIS_param_Modulos[1,1] == 1) 
    {AsIS_Logs1 = matrix(c("1", "Parametrização", "Consistência versão calculadora", "Ok", "Módulos selecionados estão ok"), nrow=1, ncol=5)} else 
    {AsIS_Logs1 = matrix(c("1", "Parametrização", "Consistência versão calculadora", "Erro", "Módulos selecionados com problema. Revisar"), nrow=1, ncol=5)}
    if(versao_calculadora == "Customizada" & sum(dataset_ASIS_param_Modulos) > 1) 
    {AsIS_Logs1 = matrix(c("1", "Parametrização", "Consistência versão calculadora", "Ok", "Módulos selecionados estão ok"), nrow=1, ncol=5)} else 
    {AsIS_Logs1 = matrix(c("1", "Parametrização", "Consistência versão calculadora", "Erro", "Módulos selecionados com problema. Revisar"), nrow=1, ncol=5)}
    colnames(AsIS_Logs1) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")   
    # Agrega Logs
    AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs1)
    
    ### Log/Check 02 - Preenchimento Planilha INSS ASIS <Criado em: jun/2018>
    Oficial_INSS_COL_NAMES = c("Item", "Número.do.Benefício","Número.da.CAT","Espécie.de.Benefício","Renda.Mensal.Inicial.RMI..R..","Data.de.Despacho.do.Benefício..DDB.",
                               "Data.Inicial.de.Contabilização..fixada.ou.real.","Data.Final.de.Contabilização..fixada..real.ou.projetada.","Duração.Expectativa.de.Tempo..meses.",
                               "Total.Pago.Projeção..R..","Data.de.Nascimento.do.Beneficiário","Tipo.do.Dependente")
    # Testa se os nomes das colunas são identicas ao template de dados
    if(identical(Oficial_INSS_COL_NAMES, colnames(dataset_ASIS_Simple_INSS)))
    {AsIS_Logs2 = matrix(c("2", "Simplificado_INSS", "Consistência preenchimento dos dados", "Ok", "Dados carregados estão ok"), nrow=1, ncol=5)} else
    {AsIS_Logs2 = matrix(c("2", "Simplificado_INSS", "Consistência preenchimento dos dados", "Erro", "As colunas dos dados do INSS não estão de acordo com template"), nrow=1, ncol=5)}
    colnames(AsIS_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
    # Agrega Logs
    AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs2)
    
    ### Log/Check 02.1 - Preenchimento Planilha INSS ASIS - verifica se dados analiticos do FapWeb foram preenchidos corretamente <Criado em: jun/2018>
    # Testa o ultimo ano de beneficios do INSS coletados do FAPWEB é igual ao ano atual menos 2 (e.g. se estamos em 2018 devem haver dados até 2016, exceto quando a empresa não possui eventos no FAP do ano vigente)
    if(INSS_Ano_Max >= (lubridate::year(today())-2))
      {AsIS_Logs21 = matrix(c("2.1", "Simplificado_INSS", "Consistência ano mais recente dos eventos", "Ok", "Dados carregados estão ok"), nrow=1, ncol=5)} else
      {AsIS_Logs21 = matrix(c("2.1", "Simplificado_INSS", "Consistência ano mais recente dos eventos", "Erro", "OS eventos observados são antigos. Revisar dados coletados do FAPWEB (ver manual)"), nrow=1, ncol=5)}
    colnames(AsIS_Logs21) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
    # Agrega Logs
    AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs21)

    ### Log/Check 3 - Preenchimento Planilha SIMPLIFICADO - OUTROS <Criado em: jun/2018>
    # Variáveis que são utilizadas em regressão e para cálculo de prob. ocorrencia de eventos devem possuir dados preenchidos para os últimos 3 anos (usualmente 5 pontos de dados contínuos). 
    # Para cada variável verifica se o ultimo ano foi preenchio e os ultimos 3 quando aplicável.
    # CustoMDO
    if(is.na(DB_Calc[10,1]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher o último ano para a variável - Despesa média com Pessoal por Hora"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # CustoMedSubstitu
    if(is.na(DB_Calc[10,2]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher o último ano para a variável - Custo Médio Subst."), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # DesligamentosInvoluntarios (3 últimos anos)
    if(is.na(DB_Calc[10,3]) || is.na(DB_Calc[9,3]) || is.na(DB_Calc[8,3]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Desligamentos Involuntários"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Aux_NroTotalDesligamentos (3 últimos anos)
    if(is.na(DB_Calc[10,4]) || is.na(DB_Calc[9,4]) || is.na(DB_Calc[8,4]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro Total Desligamentos"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Aux_TotalDiasAfast_Men15 (pode ser ARBITRADA) 
    if(is.na(DB_Calc[10,5])){
       if(is.na(DB_ASIS_Completo_Arbitrado[11,1]) || is.na(DB_ASIS_Completo_Arbitrado[11,2]) || is.na(DB_ASIS_Completo_Arbitrado[11,3]))
         {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher o último ano (ou arbitre) para a variável - Nro. Total Dias Afast. Menor 15d"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # Funcionarios (3 últimos anos)
    if(is.na(DB_Calc[10,6]) || is.na(DB_Calc[9,6]) || is.na(DB_Calc[8,6]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Funcionarios"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Aux_TotalHorasTrabalhadas (pode ser ARBITRADA)
    if(is.na(DB_Calc[10,7])){
      if(is.na(DB_ASIS_Completo_Arbitrado[13,1]) || is.na(DB_ASIS_Completo_Arbitrado[13,2]) || is.na(DB_ASIS_Completo_Arbitrado[13,3]))
        {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher o último ano (ou arbitre) para a variável - Total Horas Trabalhadas no Ano"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # HorasHomemExposicaoRisco (pode ser ARBITRADA)
    if(is.na(DB_Calc[10,8])){
      if(is.na(DB_ASIS_Completo_Arbitrado[14,1]) || is.na(DB_ASIS_Completo_Arbitrado[14,2]) || is.na(DB_ASIS_Completo_Arbitrado[14,3]))
        {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher o último ano (ou arbitre) para a variável - Total Horas Homem em Exposição ao Risco no Ano"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # Nev_Afmaior15_DoenOcup (3 últimos anos) (pode ser ARBITRADA)
    if(is.na(DB_Calc[10,9]) || is.na(DB_Calc[9,9]) || is.na(DB_Calc[8,9])){
      if(is.na(DB_ASIS_Completo_Arbitrado[15,1]) || is.na(DB_ASIS_Completo_Arbitrado[15,2]) || is.na(DB_ASIS_Completo_Arbitrado[15,3]))
        {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos (ou arbitre) para a variável - Nro. Afast. > 15d - DoenOcup"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # Nev_Afmaior15_NRelac (3 últimos anos)
    if(is.na(DB_Calc[10,10]) || is.na(DB_Calc[9,10]) || is.na(DB_Calc[8,10]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Afast. > 15d - NRelac"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Afmaior15_Tipico (3 últimos anos) (pode ser ARBITRADA)
    if(is.na(DB_Calc[10,11]) || is.na(DB_Calc[9,11]) || is.na(DB_Calc[8,11])){
       if(is.na(DB_ASIS_Completo_Arbitrado[17,1]) || is.na(DB_ASIS_Completo_Arbitrado[17,2]) || is.na(DB_ASIS_Completo_Arbitrado[17,3]))
         {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos (ou arbitre) para a variável - Nro. Afast. > 15d - Típico"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # Nev_Afmaior15_Trajeto (3 últimos anos)
    if(is.na(DB_Calc[10,12]) || is.na(DB_Calc[9,12]) || is.na(DB_Calc[8,12]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Afast. > 15d - Trajeto"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Afmenor15_DoenOcup (3 últimos anos) (pode ser ARBITRADA)
    if(is.na(DB_Calc[10,13]) || is.na(DB_Calc[9,13]) || is.na(DB_Calc[8,13])){
      if(is.na(DB_ASIS_Completo_Arbitrado[19,1]) || is.na(DB_ASIS_Completo_Arbitrado[19,2]) || is.na(DB_ASIS_Completo_Arbitrado[19,3]))
        {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos (ou arbitre) para a variável - Nro. Afast. < 15d - DoenOcup"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # Nev_Afmenor15_NRelac (3 últimos anos)
    if(is.na(DB_Calc[10,14]) || is.na(DB_Calc[9,14]) || is.na(DB_Calc[8,14]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Afast. < 15d - NRelac"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Afmenor15_Tipico (3 últimos anos) (pode ser ARBITRADA)
    if(is.na(DB_Calc[10,15]) || is.na(DB_Calc[9,15]) || is.na(DB_Calc[8,15])){
      if(is.na(DB_ASIS_Completo_Arbitrado[21,1]) || is.na(DB_ASIS_Completo_Arbitrado[21,2]) || is.na(DB_ASIS_Completo_Arbitrado[21,3]))
        {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos (ou arbitre) para a variável - Nro. Afast. < 15d - Típico"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # Nev_Afmenor15_Trajeto (3 últimos anos)
    if(is.na(DB_Calc[10,16]) || is.na(DB_Calc[9,16]) || is.na(DB_Calc[8,16]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Afast. < 15d - Trajeto"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Obito_DoenOcup (3 últimos anos)
    if(is.na(DB_Calc[10,17]) || is.na(DB_Calc[9,17]) || is.na(DB_Calc[8,17]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Óbito - DoenOcup"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Obito_NRelac (3 últimos anos)
    if(is.na(DB_Calc[10,18]) || is.na(DB_Calc[9,18]) || is.na(DB_Calc[8,18]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Óbito - NRelac"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Obito_Tipico (3 últimos anos) (pode ser ARBITRADA)
    if(is.na(DB_Calc[10,19]) || is.na(DB_Calc[9,19]) || is.na(DB_Calc[8,19])){
      if(is.na(DB_ASIS_Completo_Arbitrado[25,1]))
        {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos (ou arbitre) para a variável - Nro. Óbito - Típico"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # Nev_Obito_Trajeto (3 últimos anos)
    if(is.na(DB_Calc[10,20]) || is.na(DB_Calc[9,20]) || is.na(DB_Calc[8,20]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Óbito - Trajeto"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Safast_DoenOcup (3 últimos anos)
    if(is.na(DB_Calc[10,21]) || is.na(DB_Calc[9,21]) || is.na(DB_Calc[8,21]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Eventos Sem Afast. - DoenOcup"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Safast_NRelac (3 últimos anos)
    if(is.na(DB_Calc[10,22]) || is.na(DB_Calc[9,22]) || is.na(DB_Calc[8,22]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Eventos Sem Afast. - NRelac"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Safast_Tipico (3 últimos anos)
    if(is.na(DB_Calc[10,23]) || is.na(DB_Calc[9,23]) || is.na(DB_Calc[8,23]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Eventos Sem Afast. - Típico"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Nev_Safast_Trajeto (3 últimos anos)
    if(is.na(DB_Calc[10,24]) || is.na(DB_Calc[9,24]) || is.na(DB_Calc[8,24]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Nro. Eventos Sem Afast. - Trajeto"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # Aux_NroTotalDias_Faltas (3 últimos anos) (pode ser ARBITRADA)
    if(is.na(DB_Calc[10,25]) || is.na(DB_Calc[9,25]) || is.na(DB_Calc[8,25])){
      if(is.na(DB_ASIS_Completo_Arbitrado[31,1]) || is.na(DB_ASIS_Completo_Arbitrado[31,2]) || is.na(DB_ASIS_Completo_Arbitrado[31,3]))
        {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos (ou arbitre) para a variável - Nro. Total Dias em Faltas"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # TaxaFrequencia
    if(is.na(DB_Calc[10,26]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher o último ano para a variável - Taxa de Frequência"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # TaxaGravidade (3 últimos anos)
    if(is.na(DB_Calc[10,27]) || is.na(DB_Calc[9,27]) || is.na(DB_Calc[8,27]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Taxa de Gravidade"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # VarPIB (3 últimos anos)
    if(is.na(DB_Calc[10,28]) || is.na(DB_Calc[9,28]) || is.na(DB_Calc[8,28]))
    {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos três anos para a variável - Var. percentual PIB Brasil"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}
    # DiasUteis (pode ser ARBITRADA)
    if(is.na(DB_Calc[10,29])){
      if(is.na(DB_ASIS_Completo_Arbitrado[35,1]))
        {AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Erro", "Preencher o último ano (ou arbitre) para a variável - Dias Úteis no Ano"), nrow=1, ncol=5); colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)}}
    # Caso todos os testes de preenchimento não apresentar erro, informar no quadro de log
    if(length(AsIS_Logs[,1][AsIS_Logs[,1] == 3]) == 0){
      AsIS_Logs3 = matrix(c("3", "Simplificado_Outros", "Consistência preenchimento dos dados", "Ok", "Preenchimento dos dados está adequado"), nrow=1, ncol=5)
      colnames(AsIS_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
      AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs3)
    }

    ### Log/Check 04 - Preenchimento Planilha FAP ASIS <Criado em: jun/2018>
    # Testa se pelo menos 3 anos foram preenchiods (ultimos três)
    if(DB_ASIS_param_Modulos$BeneficioFAP == 1){ 
      if((sum(!is.na(dataset_ASIS_Simp_FAP[,10])) == 13) & (sum(!is.na(dataset_ASIS_Simp_FAP[,9])) == 13) & (sum(!is.na(dataset_ASIS_Simp_FAP[,8])) == 13))
      {AsIS_Logs41 = matrix(c("4", "Simplificado_FAP", "Consistência preenchimento dos dados", "Ok", "Dados carregados do FAP estão ok"), nrow=1, ncol=5)} else
      {AsIS_Logs41 = matrix(c("4", "Simplificado_FAP", "Consistência preenchimento dos dados", "Erro", "Informar dados dos três últimos anos do FAP para todas as variáveis. Consultar manual"), nrow=1, ncol=5)}} else
      {AsIS_Logs41 = matrix(c("4", "Simplificado_FAP", "Consistência preenchimento dos dados", "Ok", "Dimensão não foi selecionada"), nrow=1, ncol=5)}
    colnames(AsIS_Logs41) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
    # Agrega Logs
    AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs41)
    
    ### Log/Check 5 - Preenchimento Planilha Custom_ReduçõesFiscais <Criado em: jun/2018>
    # Testa se pelo menos uma multa foi preenchida, caso o módulo tenha sido selecionado
    if(DB_ASIS_param_Modulos$BeneficioMultas == 1){
      if((!is.na(sum(dataset_ASIS_multas[1,10])) == 1 & !is.na(sum(dataset_ASIS_multas[6,10])) == 1) | (!is.na(sum(dataset_ASIS_multas[1,11])) == 1 & !is.na(sum(dataset_ASIS_multas[6,11])) == 1))
      {AsIS_Logs5 = matrix(c("5", "Custom_ReduçõesFiscais", "Consistência preenchimento dos dados", "Ok", "Dados carregados. Pelo menos uma multa foi considerada"), nrow=1, ncol=5)} else
      {AsIS_Logs5 = matrix(c("5", "Custom_ReduçõesFiscais", "Consistência preenchimento dos dados", "Erro", "Pelo menos uma multa deve ser informada"), nrow=1, ncol=5)}} else
      {AsIS_Logs5 = matrix(c("5", "Custom_ReduçõesFiscais", "Consistência preenchimento dos dados", "Ok", "Dimensão não foi selecionada"), nrow=1, ncol=5)}
    colnames(AsIS_Logs5) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
    # Agrega Logs
    AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs5)
    
    ### Log/Check 6 - Preenchimento Planilha Custom_DespesasEvitáveis <Criado em: jun/2018>
    ### Dimensão: Despesas com Reclamatórias Trabalhistas
    # (53/A59) Despesa Média com Reclamatória Trabalhista (CustoMedioReclamatorias) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioReclamatorias == 1){
      if(is.na(DB_Calc[10,53])){
        if(is.na(DB_ASIS_Completo_Arbitrado[59,1]) || is.na(DB_ASIS_Completo_Arbitrado[59,2]) || is.na(DB_ASIS_Completo_Arbitrado[59,3]))
          {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Despesa Média com Reclamatória Trabalhista"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    # (54/A60) Probabilidade do Funcionário Ajuizar e Ganhar Ação Trabalhista (PReclamatoria) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioReclamatorias == 1){
      if(is.na(DB_Calc[10,54])){
        if(is.na(DB_ASIS_Completo_Arbitrado[60,1]) || is.na(DB_ASIS_Completo_Arbitrado[60,2]) || is.na(DB_ASIS_Completo_Arbitrado[60,3]))
          {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Probabilidade do Funcionário Ajuizar e Ganhar Ação Trabalhista"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    ### Dimensão: Ações Regressivas INSS
    # (55/A61) Número Total de Ações Regressivas do INSS (Aux_NroAcoesRegre) / (últimos três anos) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioAcoesRegressivasINSS == 1){
      if(is.na(DB_Calc[10,55]) || is.na(DB_Calc[9,55]) || is.na(DB_Calc[8,55])){
        if(is.na(DB_ASIS_Completo_Arbitrado[61,1]) || is.na(DB_ASIS_Completo_Arbitrado[61,2]) || is.na(DB_ASIS_Completo_Arbitrado[61,3]))
          {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Preencher os últimos 3 anos (ou arbitre) para a variável - Número Total de Ações Regressivas do INSS"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    ### Dimensão: Despesas Médicas
    # (56/A62) Despesa Médica Total (Aux_DespesaMedicaTotal) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioDespesasMedicas == 1){
      if(is.na(DB_Calc[10,56])){
        if(is.na(DB_ASIS_Completo_Arbitrado[62,1]) || is.na(DB_ASIS_Completo_Arbitrado[62,2]) || is.na(DB_ASIS_Completo_Arbitrado[62,3]))
        {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Despesa Médica Total"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    ### Dimensão: Reajustes Plano de Saúde
    # (57/A63) Despesas Total com Plano de Saúde (DespesasPlanoInicial) / (últimos 4 anos)
    if(DB_ASIS_param_Modulos$BeneficioPlanodeSaude == 1){
      if(is.na(DB_Calc[10,57]) || is.na(DB_Calc[9,57]) || is.na(DB_Calc[8,57]) || is.na(DB_Calc[7,57]))
        {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher os últimos 4 anos (ou desabilite a categoria) para a variável - Despesas Total com Plano de Saúde"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}
    ### Dimensão: Interrupção Operacional (Acidente/Morte)
    # (58/A64) Número Total de Dias por Interrupção Operacional devido à Óbitos (DiasInterrupcaoAcidenteObito) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioInterrupcaoAcidentes == 1){
      if(is.na(DB_Calc[10,58])){
        if(is.na(DB_ASIS_Completo_Arbitrado[64,1]) || is.na(DB_ASIS_Completo_Arbitrado[64,2]) || is.na(DB_ASIS_Completo_Arbitrado[64,3]))
        {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Nro. Total de Dias por Interrupção Op. devido à Óbitos"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    # (59/A65) Número Total de Dias por Interrupção Operacional devido à outro Acidentes (DiasInterrupcaoAcidenteOutros) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioInterrupcaoAcidentes == 1){
      if(is.na(DB_Calc[10,59])){
        if(is.na(DB_ASIS_Completo_Arbitrado[65,1]) || is.na(DB_ASIS_Completo_Arbitrado[65,2]) || is.na(DB_ASIS_Completo_Arbitrado[65,3]))
        {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Nro. Total de Dias por Interrupção Op. à outros Acidentes"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    # (60/A66) Lucro Cessante Total por Interrupções - Óbitos (Aux_LucroCessanteTotal_Obitos) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioInterrupcaoAcidentes == 1){
      if(is.na(DB_Calc[10,60])){
        if(is.na(DB_ASIS_Completo_Arbitrado[66,1]) || is.na(DB_ASIS_Completo_Arbitrado[66,2]) || is.na(DB_ASIS_Completo_Arbitrado[66,3]))
        {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Lucro Cessante Total por Interrupções - Óbitos"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    # (61/A67) Lucro Cessante Total por Interrupções - Outros Acidentes (Aux_LucroCessanteTotal_OutrosAcid) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioInterrupcaoAcidentes == 1){
      if(is.na(DB_Calc[10,61])){
        if(is.na(DB_ASIS_Completo_Arbitrado[67,1]) || is.na(DB_ASIS_Completo_Arbitrado[67,2]) || is.na(DB_ASIS_Completo_Arbitrado[67,3]))
        {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Lucro Cessante Total por Interrupções - Outros Acidentes"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    ### Dimensão: Interdições por Fiscalização
    # (62/A68) Número de Eventos de Interdição da Empresa (EventoInterdicao) (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioInterdicaoFiscalizacao == 1){
      if(is.na(DB_Calc[10,62])){
        if(is.na(DB_ASIS_Completo_Arbitrado[68,1]))
        {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Número de Eventos de Interdição da Empresa"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    # (63/A69) Lucro Cessante Médio por Interrupção - Fiscalização (LucroCessanteInterdicaoFiscalizacao) (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioInterdicaoFiscalizacao == 1){
      if(is.na(DB_Calc[10,63])){
        if(is.na(DB_ASIS_Completo_Arbitrado[69,1]) || is.na(DB_ASIS_Completo_Arbitrado[69,2]) || is.na(DB_ASIS_Completo_Arbitrado[69,3]))
        {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Lucro Cessante Médio por Interrupção - Fiscalização"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    ### Dimensão: Reabilitação do Trabalhador
    # (64/A70) Despesa Média com Reabilitação de Trabalhador (CustoMedioReabilitacao) (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioReabilitacao == 1){
      if(is.na(DB_Calc[10,64])){
        if(is.na(DB_ASIS_Completo_Arbitrado[70,1]) || is.na(DB_ASIS_Completo_Arbitrado[70,2]) || is.na(DB_ASIS_Completo_Arbitrado[70,3]))
        {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Despesa Média com Reabilitação de Trabalhador"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    # (65/A71) Número Total de Trabalhadores Reabilitados (Aux_NroTotalReabilitados) (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioReabilitacao == 1){
      if(is.na(DB_Calc[10,65])){
        if(is.na(DB_ASIS_Completo_Arbitrado[71,1]) || is.na(DB_ASIS_Completo_Arbitrado[71,2]) || is.na(DB_ASIS_Completo_Arbitrado[71,3]))
          {AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Número Total de Trabalhadores Reabilitados"), nrow=1, ncol=5); colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)}}}
    # Caso todos os testes de preenchimento não apresentar erro, informar no quadro de log
    if(length(AsIS_Logs[,1][AsIS_Logs[,1] == 6]) == 0){
      AsIS_Logs6 = matrix(c("6", "Custom_DespesasEvitáveis", "Consistência preenchimento dos dados", "Ok", "Preenchimento dos dados está adequado"), nrow=1, ncol=5)
      colnames(AsIS_Logs6) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
      AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs6)
    }    
    
    ### Log/Check 7 - Preenchimento Planilha Custom_MelhorUsoRecursos <Criado em: jun/2018>
    ### Dimensão: Presenteísmo
    # (66/A72) Percentual de Presenteísmo (PercPresenteismo) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioPresenteismo == 1){
      if(is.na(DB_Calc[10,66])){
        if(is.na(DB_ASIS_Completo_Arbitrado[72,1]) || is.na(DB_ASIS_Completo_Arbitrado[72,2]) || is.na(DB_ASIS_Completo_Arbitrado[72,3]))
        {AsIS_Logs7 = matrix(c("7", "Custom_MelhorUsoRecursos", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Percentual de Presenteísmo"), nrow=1, ncol=5); colnames(AsIS_Logs7) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs7)}}}
    ### Dimensão: Matéria-prima, insumos, equipamentos da operação
    # (67/A73) Despesa Adicional Total com Matéria Prima e Insumos (Aux_DespTotal_MPeInsumos) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioMPInsumos == 1){
      if(is.na(DB_Calc[10,67])){
        if(is.na(DB_ASIS_Completo_Arbitrado[73,1]) || is.na(DB_ASIS_Completo_Arbitrado[73,2]) || is.na(DB_ASIS_Completo_Arbitrado[73,3]))
        {AsIS_Logs7 = matrix(c("7", "Custom_MelhorUsoRecursos", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Despesa Adicional Total com Matéria Prima e Insumos"), nrow=1, ncol=5); colnames(AsIS_Logs7) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs7)}}}
    ### Dimensão: Refugo e Retrabalho
    # (68/A74) Despesa Adicional Total com Refugo e Retrabalho (Aux_DespTotal_RefugoeRetrabalho) / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioRefugoERetrabalho == 1){
      if(is.na(DB_Calc[10,68])){
        if(is.na(DB_ASIS_Completo_Arbitrado[74,1]) || is.na(DB_ASIS_Completo_Arbitrado[74,2]) || is.na(DB_ASIS_Completo_Arbitrado[74,3]))
        {AsIS_Logs7 = matrix(c("7", "Custom_MelhorUsoRecursos", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Despesa Adicional Total com Refugo e Retrabalho"), nrow=1, ncol=5); colnames(AsIS_Logs7) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs7)}}}
    # Caso todos os testes de preenchimento não apresentar erro, informar no quadro de log
    if(length(AsIS_Logs[,1][AsIS_Logs[,1] == 7]) == 0){
      AsIS_Logs7 = matrix(c("7", "Custom_MelhorUsoRecursos", "Consistência preenchimento dos dados", "Ok", "Preenchimento dos dados está adequado"), nrow=1, ncol=5)
      colnames(AsIS_Logs7) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
      AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs7)
    }        
    
    ###Log/Check 8 - Preenchimento Planilha Custom_Intangível <Criado em: jun/2018>
    ### Dimensão: Imagem da Empresa (Tempo de contratação)
    # (69/A75) Tempo Médio de Contratação / (3 últimos anos)
    if(DB_ASIS_param_Modulos$BeneficioImagemContratacao == 1){
      if(is.na(DB_Calc[10,69]) || is.na(DB_Calc[9,69]) || is.na(DB_Calc[8,69]))
        {AsIS_Logs8 = matrix(c("8", "Custom_Intangível", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher os últimos 3 anos (ou desabilite a dimensão) para a variável - Tempo Médio de Contratação"), nrow=1, ncol=5); colnames(AsIS_Logs8) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs8)}}
    # (70/A76) Despesa Média para Contratação de um Funcionário / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioImagemContratacao == 1){
      if(is.na(DB_Calc[10,70])){
        if(is.na(DB_ASIS_Completo_Arbitrado[76,1]) || is.na(DB_ASIS_Completo_Arbitrado[76,2]) || is.na(DB_ASIS_Completo_Arbitrado[76,3]))
        {AsIS_Logs8 = matrix(c("8", "Custom_Intangível", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Despesa Média para Contratação de um Funcionário"), nrow=1, ncol=5); colnames(AsIS_Logs8) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs8)}}}
    ### Dimensão: Benefício Engajamento e Clima organizacional
    # (71/A77) Despesa Média com Substituição de um Funcionário que Solicitou Desligamento da Empresa Voluntariamente / (pode ARBITRAR)
    if(DB_ASIS_param_Modulos$BeneficioClima == 1){
      if(is.na(DB_Calc[10,71])){
        if(is.na(DB_ASIS_Completo_Arbitrado[77,1]) || is.na(DB_ASIS_Completo_Arbitrado[77,2]) || is.na(DB_ASIS_Completo_Arbitrado[77,3]))
        {AsIS_Logs8 = matrix(c("8", "Custom_Intangível", "Consistência preenchimento dos dados", "Erro", "Desabilitar a dimensão ou Preencher o último ano (ou arbitre) para a variável - Despesa Média com Substituição de um Funcionário que Solicitou Desligamento"), nrow=1, ncol=5); colnames(AsIS_Logs8) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs8)}}}
    # Caso todos os testes de preenchimento não apresentar erro, informar no quadro de log
    if(length(AsIS_Logs[,1][AsIS_Logs[,1] == 8]) == 0){
      AsIS_Logs8 = matrix(c("8", "Custom_Intangível", "Consistência preenchimento dos dados", "Ok", "Preenchimento dos dados está adequado"), nrow=1, ncol=5)
      colnames(AsIS_Logs8) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
      AsIS_Logs = rbind(AsIS_Logs, AsIS_Logs8)
    }      

    #### 1.8 Sintese dos Objetos e salvar em csv para eventual necessidade de debug ####    

    write.csv2(eventos_pdf, "Outputs/Nro_Eventos.csv")
    write.csv2(eventos_pdf_prob, "Outputs/Prob_Eventos.csv")

    showNotification("Processamento dos dados finalizado", type = c("warning"), duration = 2)
    
    # Objeto teste a ser impresso na tela
    mensagem1 = c("Processamento finalizado")
    
    # Flag indica que dados ASIS foram processados
    Flag_asis = 1

    #### 1.10 Lista de objetos para serem utilizados em outras funções reativas ####
    ASIS_List <- list(
      AsIS_Logs = AsIS_Logs,
      mensagem1 = mensagem1,
      dataset_ASIS_param_Modulos = dataset_ASIS_param_Modulos,
      dataset_ASIS_param_taxadesconto = dataset_ASIS_param_taxadesconto,
      dataset_ASIS_param_cadastEmp = dataset_ASIS_param_cadastEmp, 
      DB_Calc_stats = DB_Calc_stats,
      DB_ASIS_Completo_Arbitrado = DB_ASIS_Completo_Arbitrado,
      eventos_pdf = eventos_pdf,
      eventos_pdf_prob = eventos_pdf_prob,
      Funcionarios_ultimoano = Funcionarios_ultimoano,
      dados_inss = dados_inss,
      check_variaveis1 = check_variaveis1,
      eventos_pdf_arb = eventos_pdf_arb,
      Flag_asis = Flag_asis
    ) 
    
  }) #Fim função reativa ASIS (Parte 1)
  
  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  # Parte 2: Modelagem Dados de Entrada das Iniciativas -----
  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  parte2 <- reactive({
    
    #### 2.1 Carregar os Dados ####

    # Leitura do arquivo com as iniciativas
    inFile3 <- input$file3
    ext <- tools::file_ext(inFile3$name)
    file.rename(inFile3$datapath,
                paste(inFile3$datapath, ext, sep="."))

    # Carregar objetos gerados na leitura e processamentos dos dados ASIS (Parte1)    
    ASIS_List3 <- parte1()
    dataset_ASIS_param_Modulos <- ASIS_List3$dataset_ASIS_param_Modulos
    dataset_ASIS_param_taxadesconto <- ASIS_List3$dataset_ASIS_param_taxadesconto
    dataset_ASIS_param_cadastEmp <- ASIS_List3$dataset_ASIS_param_cadastEmp
    DB_Calc_stats <- ASIS_List3$DB_Calc_stats
    DB_ASIS_Completo_Arbitrado <- ASIS_List3$DB_ASIS_Completo_Arbitrado
    eventos_pdf <- ASIS_List3$eventos_pdf
    eventos_pdf_prob <- ASIS_List3$eventos_pdf_prob
    Funcionarios_ultimoano = ASIS_List3$Funcionarios_ultimoano
    
    #### 2.2 Ler Dados Planilha EXCEL Planilha Iniciativas (Planilha Iniciativas.xlsx) ####
    
    ### Versão do arquivo de dados
    #+++++++++++++++++++++++++++++++++++++++++++++
    versao_planilha_inic = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), 
                                                 range = "Parametrização!E3",
                                                 col_names = FALSE))
    
    ### 2.2.1. Ler Planilha de INIC - PARAMETRIZACAO
    #+++++++++++++++++++++++++++++++++++++++++++++
    dataset_INIC_AnosAvaliacao = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), 
                                                       range = "Parametrização!C7",
                                                       col_names = FALSE))
    
    colnames(dataset_INIC_AnosAvaliacao) <- c("AnosRetorno")
    
    
    dataset_INIC_Selecao = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), 
                                                 range = "Parametrização!B11:E21",
                                                 col_names = TRUE))
    
    colnames(dataset_INIC_Selecao) <- c("Iniciativa",
                                        "NomeIniciativa",
                                        "Selecionada",
                                        "AnosDelay")
    
    ### 2.2.2. Ler Planilha de INIC - PROJECOES
    #+++++++++++++++++++++++++++++++++++++++++++++
    dataset_INIC_Projetado = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), 
                                                   range = "Infos_Comuns!E6:N9",
                                                   col_names = TRUE))
    
    row.names(dataset_INIC_Projetado) <- c("Crise",
                                           "FatorCrise",
                                           "VarPIB")
    
    ### Redefinição BASELINE ASIS Calculado
    #+++++++++++++++++++++++++++++++++++++++++++++
    dataset_INIC_BASELINE = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), 
                                                  range = "Iniciativa 1!I18:I34",
                                                  col_names = FALSE))
    
    row.names(dataset_INIC_BASELINE) <- c("Nev_Afmaior15_DoenOcup",
                                          "Nev_Afmaior15_NRelac",
                                          "Nev_Afmaior15_Tipico",
                                          "Nev_Afmaior15_Trajeto",
                                          "Nev_Afmenor15_DoenOcup",
                                          "Nev_Afmenor15_NRelac",
                                          "Nev_Afmenor15_Tipico",
                                          "Nev_Afmenor15_Trajeto",
                                          "Nev_Obito_DoenOcup",
                                          "Nev_Obito_NRelac",
                                          "Nev_Obito_Tipico",
                                          "Nev_Obito_Trajeto",
                                          "Nev_Safast_DoenOcup",
                                          "Nev_Safast_NRelac",
                                          "Nev_Safast_Tipico",
                                          "Nev_Safast_Trajeto",
                                          "Aux_NroTotalDias_Faltas")
    
    ## Calcula as prob. a partir dos Baselines informados na iniciativa 1
    dataset_INIC_BASELINE_eventos = matrix(0, ncol = 1, nrow = 17)
    row.names(dataset_INIC_BASELINE_eventos) = row.names(dataset_INIC_BASELINE)
    for(v in 1:17){
      # Seleciona o número de eventos média ou último ano.
      if(dataset_INIC_BASELINE[v, ] == "Média") {dataset_INIC_BASELINE_eventos[v, 1] = eventos_pdf[v, 7]
      }                                    else {dataset_INIC_BASELINE_eventos[v, 1] = eventos_pdf[v, 6]} 
      
    }
    
    dataset_INIC_BASELINE_prob = matrix(0, ncol = 1, nrow = 17)
    row.names(dataset_INIC_BASELINE_prob) <- c("Pev_Afmaior15_DoenOcup",
                                               "Pev_Afmaior15_NRelac",
                                               "Pev_Afmaior15_Tipico",
                                               "Pev_Afmaior15_Trajeto",
                                               "Pev_Afmenor15_DoenOcup",
                                               "Pev_Afmenor15_NRelac",
                                               "Pev_Afmenor15_Tipico",
                                               "Pev_Afmenor15_Trajeto",
                                               "Pev_Obito_DoenOcup",
                                               "Pev_Obito_NRelac",
                                               "Pev_Obito_Tipico",
                                               "Pev_Obito_Trajeto",
                                               "Pev_Safast_DoenOcup",
                                               "Pev_Safast_NRelac",
                                               "Pev_Safast_Tipico",
                                               "Pev_Safast_Trajeto",
                                               "TaxaFaltas")
    for(v in 1:17){
      # Seleciona o número de eventos média ou último ano.
      if(dataset_INIC_BASELINE[v, ] == "Média") {dataset_INIC_BASELINE_prob[v, 1] = eventos_pdf_prob[v, 7]
      }                                    else {dataset_INIC_BASELINE_prob[v, 1] = eventos_pdf_prob[v, 6]} 
      
    }
    
    dataset_INIC_BASELINE = rbind(dataset_INIC_BASELINE_eventos, dataset_INIC_BASELINE_prob)

    ### 2.2.3. Ler Planilha de INIC - INICIATIVA
    #+++++++++++++++++++++++++++++++++++++++++++++
    ### Custos
    if(dataset_INIC_Selecao[1, 3] == TRUE){
      dataset_Inic1_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 1!D8:M10", col_names = TRUE))
      row.names(dataset_Inic1_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic1_Custos <- NULL}
    if(dataset_INIC_Selecao[2, 3] == TRUE){
      dataset_Inic2_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 2!D8:M10", col_names = TRUE))
      row.names(dataset_Inic2_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic2_Custos <- NULL}
    if(dataset_INIC_Selecao[3, 3] == TRUE){
      dataset_Inic3_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 3!D8:M10", col_names = TRUE))
      row.names(dataset_Inic3_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic3_Custos <- NULL}
    if(dataset_INIC_Selecao[4, 3] == TRUE){
      dataset_Inic4_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 4!D8:M10", col_names = TRUE))
      row.names(dataset_Inic4_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic4_Custos <- NULL}
    if(dataset_INIC_Selecao[5, 3] == TRUE){
      dataset_Inic5_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 5!D8:M10", col_names = TRUE))
      row.names(dataset_Inic5_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic5_Custos <- NULL}
    if(dataset_INIC_Selecao[6, 3] == TRUE){
      dataset_Inic6_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 6!D8:M10", col_names = TRUE))
      row.names(dataset_Inic6_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic6_Custos <- NULL}
    if(dataset_INIC_Selecao[7, 3] == TRUE){
      dataset_Inic7_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 7!D8:M10", col_names = TRUE))
      row.names(dataset_Inic7_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic7_Custos <- NULL}
    if(dataset_INIC_Selecao[8, 3] == TRUE){
      dataset_Inic8_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 8!D8:M10", col_names = TRUE))
      row.names(dataset_Inic8_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic8_Custos <- NULL}
    if(dataset_INIC_Selecao[9, 3] == TRUE){
      dataset_Inic9_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 9!D8:M10", col_names = TRUE))
      row.names(dataset_Inic9_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic9_Custos <- NULL}
    if(dataset_INIC_Selecao[10, 3] == TRUE){
      dataset_Inic10_Custos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 10!D8:M10", col_names = TRUE))
      row.names(dataset_Inic10_Custos) <- c("CustoInicial", "CustoManutencao")
    } else {dataset_Inic10_Custos <- NULL}
    
    ### Prob Eventos ASIS
    if(dataset_INIC_Selecao[1, 3] == TRUE){
      dataset_Inic1_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 1!C18:F34", col_names = FALSE))
      colnames(dataset_Inic1_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic1_EventosASIS <- NULL}
    if(dataset_INIC_Selecao[2, 3] == TRUE){
      dataset_Inic2_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 2!C18:F34", col_names = FALSE))
      colnames(dataset_Inic2_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic2_EventosASIS <- NULL}
    if(dataset_INIC_Selecao[3, 3] == TRUE){
      dataset_Inic3_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 3!C18:F34", col_names = FALSE))
      colnames(dataset_Inic3_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic3_EventosASIS <- NULL}
    if(dataset_INIC_Selecao[4, 3] == TRUE){
      dataset_Inic4_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 4!C18:F34", col_names = FALSE))
      colnames(dataset_Inic4_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic4_EventosASIS <- NULL}
    if(dataset_INIC_Selecao[5, 3] == TRUE){
      dataset_Inic5_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 5!C18:F34", col_names = FALSE))
      colnames(dataset_Inic5_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic5_EventosASIS <- NULL}
    if(dataset_INIC_Selecao[6, 3] == TRUE){
      dataset_Inic6_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 6!C18:F34", col_names = FALSE))
      colnames(dataset_Inic6_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic6_EventosASIS <- NULL}
    if(dataset_INIC_Selecao[7, 3] == TRUE){
      dataset_Inic7_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 7!C18:F34", col_names = FALSE))
      colnames(dataset_Inic7_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic7_EventosASIS <- NULL}
    if(dataset_INIC_Selecao[8, 3] == TRUE){
      dataset_Inic8_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 8!C18:F34", col_names = FALSE))
      colnames(dataset_Inic8_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic8_EventosASIS <- NULL}
    if(dataset_INIC_Selecao[9, 3] == TRUE){
      dataset_Inic9_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 9!C18:F34", col_names = FALSE))
      colnames(dataset_Inic9_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic9_EventosASIS <- NULL}
    if(dataset_INIC_Selecao[10, 3] == TRUE){
      dataset_Inic10_EventosASIS = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 10!C18:F34", col_names = FALSE))
      colnames(dataset_Inic10_EventosASIS) <- c("Nro_UltimoAno", "Nro_Media", "Prob_UltimoAno", "Prob_Media")
    } else {dataset_Inic10_EventosASIS <- NULL}
    
    
    ### Prob. Futuras Eventos baseado dados Observado
    if(dataset_INIC_Selecao[1, 3] == TRUE){
      dataset_Inic1_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 1!K18:k34", col_names = FALSE))
      colnames(dataset_Inic1_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic1_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic1_PrEventos_Obs <- NULL}
    if(dataset_INIC_Selecao[2, 3] == TRUE){
      dataset_Inic2_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 2!K18:k34", col_names = FALSE))
      colnames(dataset_Inic2_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic2_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic2_PrEventos_Obs <- NULL}
    if(dataset_INIC_Selecao[3, 3] == TRUE){
      dataset_Inic3_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 3!K18:k34", col_names = FALSE))
      colnames(dataset_Inic3_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic3_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic3_PrEventos_Obs <- NULL}
    if(dataset_INIC_Selecao[4, 3] == TRUE){
      dataset_Inic4_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 4!K18:k34", col_names = FALSE))
      colnames(dataset_Inic4_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic4_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic4_PrEventos_Obs <- NULL}
    if(dataset_INIC_Selecao[5, 3] == TRUE){
      dataset_Inic5_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 5!K18:k34", col_names = FALSE))
      colnames(dataset_Inic5_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic5_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic5_PrEventos_Obs <- NULL}
    if(dataset_INIC_Selecao[6, 3] == TRUE){
      dataset_Inic6_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 6!K18:k34", col_names = FALSE))
      colnames(dataset_Inic6_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic6_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic6_PrEventos_Obs <- NULL}
    if(dataset_INIC_Selecao[7, 3] == TRUE){
      dataset_Inic7_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 7!K18:k34", col_names = FALSE))
      colnames(dataset_Inic7_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic7_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic7_PrEventos_Obs <- NULL}
    if(dataset_INIC_Selecao[8, 3] == TRUE){
      dataset_Inic8_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 8!K18:k34", col_names = FALSE))
      colnames(dataset_Inic8_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic8_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic8_PrEventos_Obs <- NULL}
    if(dataset_INIC_Selecao[9, 3] == TRUE){
      dataset_Inic9_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 9!K18:k34", col_names = FALSE))
      colnames(dataset_Inic9_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic9_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic9_PrEventos_Obs <- NULL}
    if(dataset_INIC_Selecao[10, 3] == TRUE){
      dataset_Inic10_PrEventos_Obs = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 10!K18:k34", col_names = FALSE))
      colnames(dataset_Inic10_PrEventos_Obs) <- c("Media_Obs")
      row.names(dataset_Inic10_PrEventos_Obs) <- c("Pev_Afmaior15_DoenOcup", "Pev_Afmaior15_NRelac", "Pev_Afmaior15_Tipico", "Pev_Afmaior15_Trajeto", "Pev_Afmenor15_DoenOcup", "Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
    } else {dataset_Inic10_PrEventos_Obs <- NULL}
    
    
    ### Prob. Futuras Eventos baseado dados arbitrados
    if(dataset_INIC_Selecao[1, 3] == TRUE){dataset_Inic1_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 1!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic1_PrEventos_Arb) > 0) {
      colnames(dataset_Inic1_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic1_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic1_PrEventos_Arb = dataset_Inic1_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic1_PrEventos_Arb[11,] = dataset_Inic1_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic1_PrEventos_Arb <- NULL}}    
    if(dataset_INIC_Selecao[2, 3] == TRUE){dataset_Inic2_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 2!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic2_PrEventos_Arb) > 0) {
      colnames(dataset_Inic2_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic2_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic2_PrEventos_Arb = dataset_Inic2_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic2_PrEventos_Arb[11,] = dataset_Inic2_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic2_PrEventos_Arb <- NULL}}    
    if(dataset_INIC_Selecao[3, 3] == TRUE){dataset_Inic3_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 3!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic3_PrEventos_Arb) > 0) {
      colnames(dataset_Inic3_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic3_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic3_PrEventos_Arb = dataset_Inic3_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic3_PrEventos_Arb[11,] = dataset_Inic3_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic3_PrEventos_Arb <- NULL}}    
    if(dataset_INIC_Selecao[4, 3] == TRUE){dataset_Inic4_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 4!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic4_PrEventos_Arb) > 0) {
      colnames(dataset_Inic4_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic4_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic4_PrEventos_Arb = dataset_Inic4_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic4_PrEventos_Arb[11,] = dataset_Inic4_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic4_PrEventos_Arb <- NULL}}   
    if(dataset_INIC_Selecao[5, 3] == TRUE){dataset_Inic5_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 5!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic5_PrEventos_Arb) > 0) {
      colnames(dataset_Inic5_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic5_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic5_PrEventos_Arb = dataset_Inic5_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic5_PrEventos_Arb[11,] = dataset_Inic5_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic5_PrEventos_Arb <- NULL}}   
    if(dataset_INIC_Selecao[6, 3] == TRUE){dataset_Inic6_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 6!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic6_PrEventos_Arb) > 0) {
      colnames(dataset_Inic6_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic6_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic6_PrEventos_Arb = dataset_Inic6_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic6_PrEventos_Arb[11,] = dataset_Inic6_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic6_PrEventos_Arb <- NULL}}   
    if(dataset_INIC_Selecao[7, 3] == TRUE){dataset_Inic7_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 7!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic7_PrEventos_Arb) > 0) {
      colnames(dataset_Inic7_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic7_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic7_PrEventos_Arb = dataset_Inic7_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic7_PrEventos_Arb[11,] = dataset_Inic7_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic7_PrEventos_Arb <- NULL}}   
    if(dataset_INIC_Selecao[8, 3] == TRUE){dataset_Inic8_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 8!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic8_PrEventos_Arb) > 0) {
      colnames(dataset_Inic8_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic8_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic8_PrEventos_Arb = dataset_Inic8_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic8_PrEventos_Arb[11,] = dataset_Inic8_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic8_PrEventos_Arb <- NULL}}  
    if(dataset_INIC_Selecao[9, 3] == TRUE){dataset_Inic9_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 9!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic9_PrEventos_Arb) > 0) {
      colnames(dataset_Inic9_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic9_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic9_PrEventos_Arb = dataset_Inic9_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic9_PrEventos_Arb[11,] = dataset_Inic9_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic9_PrEventos_Arb <- NULL}}     
    if(dataset_INIC_Selecao[10, 3] == TRUE){dataset_Inic10_PrEventos_Arb = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."), range = "Iniciativa 10!O18:Q34",col_names = FALSE))
    if(length(dataset_Inic10_PrEventos_Arb) > 0) {
      colnames(dataset_Inic10_PrEventos_Arb) <- c("Usual", "Maximo", "Minimo")
      row.names(dataset_Inic10_PrEventos_Arb) <- c("Pev_Afmaior15_DoenOcup","Pev_Afmaior15_NRelac","Pev_Afmaior15_Tipico","Pev_Afmaior15_Trajeto","Pev_Afmenor15_DoenOcup","Pev_Afmenor15_NRelac","Pev_Afmenor15_Tipico","Pev_Afmenor15_Trajeto","Pev_Obito_DoenOcup","Pev_Obito_NRelac","Pev_Obito_Tipico","Pev_Obito_Trajeto","Pev_Safast_DoenOcup","Pev_Safast_NRelac","Pev_Safast_Tipico","Pev_Safast_Trajeto","TaxaFaltas")
      ## Calculo das Pev a partir dados arbitrados para as iniciativas
      dataset_Inic10_PrEventos_Arb = dataset_Inic10_PrEventos_Arb / Funcionarios_ultimoano
      dataset_Inic10_PrEventos_Arb[11,] = dataset_Inic10_PrEventos_Arb[11,]*Funcionarios_ultimoano
    }else {dataset_Inic10_PrEventos_Arb <- NULL}}     
    
    ### Prob. Futuras Eventos Raros
    if(dataset_INIC_Selecao[1, 3] == TRUE){dataset_Inic1_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 1!I41:I46",col_names = FALSE))
    colnames(dataset_Inic1_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic1_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic1_PrEventosRaros <- NULL} 
    if(dataset_INIC_Selecao[2, 3] == TRUE){dataset_Inic2_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 2!I41:I46",col_names = FALSE))
    colnames(dataset_Inic2_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic2_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic2_PrEventosRaros <- NULL} 
    if(dataset_INIC_Selecao[3, 3] == TRUE){dataset_Inic3_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 3!I41:I46",col_names = FALSE))
    colnames(dataset_Inic3_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic3_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic3_PrEventosRaros <- NULL} 
    if(dataset_INIC_Selecao[4, 3] == TRUE){dataset_Inic4_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 4!I41:I46",col_names = FALSE))
    colnames(dataset_Inic4_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic4_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic4_PrEventosRaros <- NULL} 
    if(dataset_INIC_Selecao[5, 3] == TRUE){dataset_Inic5_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 5!I41:I46",col_names = FALSE))
    colnames(dataset_Inic5_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic5_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic5_PrEventosRaros <- NULL} 
    if(dataset_INIC_Selecao[6, 3] == TRUE){dataset_Inic6_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 6!I41:I46",col_names = FALSE))
    colnames(dataset_Inic6_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic6_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic6_PrEventosRaros <- NULL} 
    if(dataset_INIC_Selecao[7, 3] == TRUE){dataset_Inic7_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 7!I41:I46",col_names = FALSE))
    colnames(dataset_Inic7_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic7_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic7_PrEventosRaros <- NULL} 
    if(dataset_INIC_Selecao[8, 3] == TRUE){dataset_Inic8_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 8!I41:I46",col_names = FALSE))
    colnames(dataset_Inic8_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic8_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic8_PrEventosRaros <- NULL} 
    if(dataset_INIC_Selecao[9, 3] == TRUE){dataset_Inic9_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 9!I41:I46",col_names = FALSE))
    colnames(dataset_Inic9_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic9_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic9_PrEventosRaros <- NULL} 
    if(dataset_INIC_Selecao[10, 3] == TRUE){dataset_Inic10_PrEventosRaros = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 10!I41:I46",col_names = FALSE))
    colnames(dataset_Inic10_PrEventosRaros) <- c("Taxa")
    row.names(dataset_Inic10_PrEventosRaros) <- c("EventoInterdicao","Multas1","Multas2","Multas3","Multas4","Multas5")
    }else {dataset_Inic10_PrEventosRaros <- NULL} 
    
    ### Outros Ganhos
    if(dataset_INIC_Selecao[1, 3] == TRUE){dataset_Inic1_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 1!I52:K58", col_names = FALSE))
    colnames(dataset_Inic1_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic1_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic1_Ganhos <- NULL} 
    if(dataset_INIC_Selecao[2, 3] == TRUE){dataset_Inic2_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 2!I52:K58", col_names = FALSE))
    colnames(dataset_Inic2_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic2_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic2_Ganhos <- NULL} 
    if(dataset_INIC_Selecao[3, 3] == TRUE){dataset_Inic3_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 3!I52:K58", col_names = FALSE))
    colnames(dataset_Inic3_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic3_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic3_Ganhos <- NULL} 
    if(dataset_INIC_Selecao[4, 3] == TRUE){dataset_Inic4_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 4!I52:K58", col_names = FALSE))
    colnames(dataset_Inic4_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic4_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic4_Ganhos <- NULL} 
    if(dataset_INIC_Selecao[5, 3] == TRUE){dataset_Inic5_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 5!I52:K58", col_names = FALSE))
    colnames(dataset_Inic5_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic5_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic5_Ganhos <- NULL} 
    if(dataset_INIC_Selecao[6, 3] == TRUE){dataset_Inic6_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 6!I52:K58", col_names = FALSE))
    colnames(dataset_Inic6_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic6_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic6_Ganhos <- NULL} 
    if(dataset_INIC_Selecao[7, 3] == TRUE){dataset_Inic7_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 7!I52:K58", col_names = FALSE))
    colnames(dataset_Inic7_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic7_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic7_Ganhos <- NULL} 
    if(dataset_INIC_Selecao[8, 3] == TRUE){dataset_Inic8_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 8!I52:K58", col_names = FALSE))
    colnames(dataset_Inic8_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic8_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic8_Ganhos <- NULL} 
    if(dataset_INIC_Selecao[9, 3] == TRUE){dataset_Inic9_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 9!I52:K58", col_names = FALSE))
    colnames(dataset_Inic9_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic9_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic9_Ganhos <- NULL} 
    if(dataset_INIC_Selecao[10, 3] == TRUE){dataset_Inic10_Ganhos = data.frame(read_excel(paste(inFile3$datapath, ext, sep="."),range = "Iniciativa 10!I52:K58", col_names = FALSE))
    colnames(dataset_Inic10_Ganhos) <- c("Usual", "Maximo", "Minimo")
    row.names(dataset_Inic10_Ganhos) <- c("GanhoImagemReceitaEsperado","TFrMaximaImagem", "TGrMaximaImagem","GanhoProdutividade","GanhoQualidade","PercPresenteismo","DespesasSeguroPatrimonial")
    }else {dataset_Inic10_Ganhos <- NULL} 
    
    #### 2.3 Calculo e Organização dos Dados ####  
    
    ### Geração dos Bancos de Dados Observado e Arbitrado para cada Iniciativa
    ### INICIATIVA 1
    # Replicando as contantes por 10 anos
    dataset_Inic1_PrEventos_Obs = dataset_Inic1_PrEventos_Obs[rep(names(dataset_Inic1_PrEventos_Obs), 10)]
    dataset_Inic1_PrEventosRaros = dataset_Inic1_PrEventosRaros[rep(names(dataset_Inic1_PrEventosRaros), 10)]
    # Ajustando os nomes das colunas de todas as tabelas
    anos_bd_init <- c(lubridate::year(Sys.Date())+1,
                      lubridate::year(Sys.Date())+2,
                      lubridate::year(Sys.Date())+3,
                      lubridate::year(Sys.Date())+4,
                      lubridate::year(Sys.Date())+5,
                      lubridate::year(Sys.Date())+6,
                      lubridate::year(Sys.Date())+7,
                      lubridate::year(Sys.Date())+8,
                      lubridate::year(Sys.Date())+9,
                      lubridate::year(Sys.Date())+10)
    
    colnames(dataset_Inic1_PrEventos_Obs) <- anos_bd_init
    colnames(dataset_Inic1_PrEventosRaros) <- anos_bd_init
    colnames(dataset_INIC_Projetado) <- anos_bd_init
    colnames(dataset_Inic1_Custos) <- anos_bd_init
    
    DB_INIC_1 = rbind(dataset_INIC_Projetado,
                      dataset_Inic1_Custos,
                      dataset_Inic1_PrEventos_Obs,
                      dataset_Inic1_PrEventosRaros
    )
    
    DB_ARB_INIC_1 = rbind(dataset_Inic1_PrEventos_Arb,
                          dataset_Inic1_Ganhos)
    
    ### Calculo do Custo Total
    DB_INIC_1 = data.frame(t(DB_INIC_1))
    DB_ARB_INIC_1 = data.frame(t(DB_ARB_INIC_1))
    
    CustoTotal = DB_INIC_1$CustoInicial + DB_INIC_1$CustoManutencao
    DB_INIC_1 = cbind(DB_INIC_1, CustoTotal)  
    
    ### INICIATIVA 2
    if(dataset_INIC_Selecao[2, 3] == TRUE){
      # Replicando as contantes por 20 anos
      dataset_Inic2_PrEventos_Obs = dataset_Inic2_PrEventos_Obs[rep(names(dataset_Inic2_PrEventos_Obs), 10)]
      dataset_Inic2_PrEventosRaros = dataset_Inic2_PrEventosRaros[rep(names(dataset_Inic2_PrEventosRaros), 10)]
      # Ajustando os nomes das colunas de todas as tabelas
      colnames(dataset_Inic2_PrEventos_Obs) <- anos_bd_init
      colnames(dataset_Inic2_PrEventosRaros) <- anos_bd_init
      colnames(dataset_INIC_Projetado) <- anos_bd_init
      colnames(dataset_Inic2_Custos) <- anos_bd_init
      
      DB_INIC_2 = rbind(dataset_INIC_Projetado,
                        dataset_Inic2_Custos,
                        dataset_Inic2_PrEventos_Obs,
                        dataset_Inic2_PrEventosRaros
      )
      
      DB_ARB_INIC_2 = rbind(dataset_Inic2_PrEventos_Arb,
                            dataset_Inic2_Ganhos)
      
      ### Calculo do Custo Total
      DB_INIC_2 = data.frame(t(DB_INIC_2))
      DB_ARB_INIC_2 = data.frame(t(DB_ARB_INIC_2))
      
      CustoTotal = DB_INIC_2$CustoInicial + DB_INIC_2$CustoManutencao
      DB_INIC_2 = cbind(DB_INIC_2, CustoTotal)  
    } else {c(DB_INIC_2 <- NULL, DB_ARB_INIC_2 <- NULL)}
    
    ### INICIATIVA 3
    if(dataset_INIC_Selecao[3, 3] == TRUE){
      # Replicando as contantes por 30 anos
      dataset_Inic3_PrEventos_Obs = dataset_Inic3_PrEventos_Obs[rep(names(dataset_Inic3_PrEventos_Obs), 10)]
      dataset_Inic3_PrEventosRaros = dataset_Inic3_PrEventosRaros[rep(names(dataset_Inic3_PrEventosRaros), 10)]
      # Ajustando os nomes das colunas de todas as tabelas
      colnames(dataset_Inic3_PrEventos_Obs) <- anos_bd_init
      colnames(dataset_Inic3_PrEventosRaros) <- anos_bd_init
      colnames(dataset_INIC_Projetado) <- anos_bd_init
      colnames(dataset_Inic3_Custos) <- anos_bd_init
      
      DB_INIC_3 = rbind(dataset_INIC_Projetado,
                        dataset_Inic3_Custos,
                        dataset_Inic3_PrEventos_Obs,
                        dataset_Inic3_PrEventosRaros
      )
      
      DB_ARB_INIC_3 = rbind(dataset_Inic3_PrEventos_Arb,
                            dataset_Inic3_Ganhos)
      
      ### Calculo do Custo Total
      DB_INIC_3 = data.frame(t(DB_INIC_3))
      DB_ARB_INIC_3 = data.frame(t(DB_ARB_INIC_3))
      
      CustoTotal = DB_INIC_3$CustoInicial + DB_INIC_3$CustoManutencao
      DB_INIC_3 = cbind(DB_INIC_3, CustoTotal)  
    } else {c(DB_INIC_3 <- NULL, DB_ARB_INIC_3 <- NULL)}
    
    ### INICIATIVA 4
    if(dataset_INIC_Selecao[4, 3] == TRUE){
      # Replicando as contantes por 40 anos
      dataset_Inic4_PrEventos_Obs = dataset_Inic4_PrEventos_Obs[rep(names(dataset_Inic4_PrEventos_Obs), 10)]
      dataset_Inic4_PrEventosRaros = dataset_Inic4_PrEventosRaros[rep(names(dataset_Inic4_PrEventosRaros), 10)]
      # Ajustando os nomes das colunas de todas as tabelas
      colnames(dataset_Inic4_PrEventos_Obs) <- anos_bd_init
      colnames(dataset_Inic4_PrEventosRaros) <- anos_bd_init
      colnames(dataset_INIC_Projetado) <- anos_bd_init
      colnames(dataset_Inic4_Custos) <- anos_bd_init
      
      DB_INIC_4 = rbind(dataset_INIC_Projetado,
                        dataset_Inic4_Custos,
                        dataset_Inic4_PrEventos_Obs,
                        dataset_Inic4_PrEventosRaros
      )
      
      DB_ARB_INIC_4 = rbind(dataset_Inic4_PrEventos_Arb,
                            dataset_Inic4_Ganhos)
      
      ### Calculo do Custo Total
      DB_INIC_4 = data.frame(t(DB_INIC_4))
      DB_ARB_INIC_4 = data.frame(t(DB_ARB_INIC_4))
      
      CustoTotal = DB_INIC_4$CustoInicial + DB_INIC_4$CustoManutencao
      DB_INIC_4 = cbind(DB_INIC_4, CustoTotal)  
    } else {c(DB_INIC_4 <- NULL, DB_ARB_INIC_4 <- NULL)}
    
    ### INICIATIVA 5
    if(dataset_INIC_Selecao[5, 3] == TRUE){
      # Replicando as contantes por 50 anos
      dataset_Inic5_PrEventos_Obs = dataset_Inic5_PrEventos_Obs[rep(names(dataset_Inic5_PrEventos_Obs), 10)]
      dataset_Inic5_PrEventosRaros = dataset_Inic5_PrEventosRaros[rep(names(dataset_Inic5_PrEventosRaros), 10)]
      # Ajustando os nomes das colunas de todas as tabelas
      colnames(dataset_Inic5_PrEventos_Obs) <- anos_bd_init
      colnames(dataset_Inic5_PrEventosRaros) <- anos_bd_init
      colnames(dataset_INIC_Projetado) <- anos_bd_init
      colnames(dataset_Inic5_Custos) <- anos_bd_init
      
      DB_INIC_5 = rbind(dataset_INIC_Projetado,
                        dataset_Inic5_Custos,
                        dataset_Inic5_PrEventos_Obs,
                        dataset_Inic5_PrEventosRaros
      )
      
      DB_ARB_INIC_5 = rbind(dataset_Inic5_PrEventos_Arb,
                            dataset_Inic5_Ganhos)
      
      ### Calculo do Custo Total
      DB_INIC_5 = data.frame(t(DB_INIC_5))
      DB_ARB_INIC_5 = data.frame(t(DB_ARB_INIC_5))
      
      CustoTotal = DB_INIC_5$CustoInicial + DB_INIC_5$CustoManutencao
      DB_INIC_5 = cbind(DB_INIC_5, CustoTotal)  
    } else {c(DB_INIC_5 <- NULL, DB_ARB_INIC_5 <- NULL)}
    
    ### INICIATIVA 6
    if(dataset_INIC_Selecao[6, 3] == TRUE){
      # Replicando as contantes por 60 anos
      dataset_Inic6_PrEventos_Obs = dataset_Inic6_PrEventos_Obs[rep(names(dataset_Inic6_PrEventos_Obs), 10)]
      dataset_Inic6_PrEventosRaros = dataset_Inic6_PrEventosRaros[rep(names(dataset_Inic6_PrEventosRaros), 10)]
      # Ajustando os nomes das colunas de todas as tabelas
      colnames(dataset_Inic6_PrEventos_Obs) <- anos_bd_init
      colnames(dataset_Inic6_PrEventosRaros) <- anos_bd_init
      colnames(dataset_INIC_Projetado) <- anos_bd_init
      colnames(dataset_Inic6_Custos) <- anos_bd_init
      
      DB_INIC_6 = rbind(dataset_INIC_Projetado,
                        dataset_Inic6_Custos,
                        dataset_Inic6_PrEventos_Obs,
                        dataset_Inic6_PrEventosRaros
      )
      
      DB_ARB_INIC_6 = rbind(dataset_Inic6_PrEventos_Arb,
                            dataset_Inic6_Ganhos)
      
      ### Calculo do Custo Total
      DB_INIC_6 = data.frame(t(DB_INIC_6))
      DB_ARB_INIC_6 = data.frame(t(DB_ARB_INIC_6))
      
      CustoTotal = DB_INIC_6$CustoInicial + DB_INIC_6$CustoManutencao
      DB_INIC_6 = cbind(DB_INIC_6, CustoTotal)  
    } else {c(DB_INIC_6 <- NULL, DB_ARB_INIC_6 <- NULL)}
    
    ### INICIATIVA 7
    if(dataset_INIC_Selecao[7, 3] == TRUE){
      # Replicando as contantes por 70 anos
      dataset_Inic7_PrEventos_Obs = dataset_Inic7_PrEventos_Obs[rep(names(dataset_Inic7_PrEventos_Obs), 10)]
      dataset_Inic7_PrEventosRaros = dataset_Inic7_PrEventosRaros[rep(names(dataset_Inic7_PrEventosRaros), 10)]
      # Ajustando os nomes das colunas de todas as tabelas
      colnames(dataset_Inic7_PrEventos_Obs) <- anos_bd_init
      colnames(dataset_Inic7_PrEventosRaros) <- anos_bd_init
      colnames(dataset_INIC_Projetado) <- anos_bd_init
      colnames(dataset_Inic7_Custos) <- anos_bd_init
      
      DB_INIC_7 = rbind(dataset_INIC_Projetado,
                        dataset_Inic7_Custos,
                        dataset_Inic7_PrEventos_Obs,
                        dataset_Inic7_PrEventosRaros
      )
      
      DB_ARB_INIC_7 = rbind(dataset_Inic7_PrEventos_Arb,
                            dataset_Inic7_Ganhos)
      
      ### Calculo do Custo Total
      DB_INIC_7 = data.frame(t(DB_INIC_7))
      DB_ARB_INIC_7 = data.frame(t(DB_ARB_INIC_7))
      
      CustoTotal = DB_INIC_7$CustoInicial + DB_INIC_7$CustoManutencao
      DB_INIC_7 = cbind(DB_INIC_7, CustoTotal)  
    } else {c(DB_INIC_7 <- NULL, DB_ARB_INIC_7 <- NULL)}
    
    ### INICIATIVA 8
    if(dataset_INIC_Selecao[8, 3] == TRUE){
      # Replicando as contantes por 80 anos
      dataset_Inic8_PrEventos_Obs = dataset_Inic8_PrEventos_Obs[rep(names(dataset_Inic8_PrEventos_Obs), 10)]
      dataset_Inic8_PrEventosRaros = dataset_Inic8_PrEventosRaros[rep(names(dataset_Inic8_PrEventosRaros), 10)]
      # Ajustando os nomes das colunas de todas as tabelas
      colnames(dataset_Inic8_PrEventos_Obs) <- anos_bd_init
      colnames(dataset_Inic8_PrEventosRaros) <- anos_bd_init
      colnames(dataset_INIC_Projetado) <- anos_bd_init
      colnames(dataset_Inic8_Custos) <- anos_bd_init
      
      DB_INIC_8 = rbind(dataset_INIC_Projetado,
                        dataset_Inic8_Custos,
                        dataset_Inic8_PrEventos_Obs,
                        dataset_Inic8_PrEventosRaros
      )
      
      DB_ARB_INIC_8 = rbind(dataset_Inic8_PrEventos_Arb,
                            dataset_Inic8_Ganhos)
      
      ### Calculo do Custo Total
      DB_INIC_8 = data.frame(t(DB_INIC_8))
      DB_ARB_INIC_8 = data.frame(t(DB_ARB_INIC_8))
      
      CustoTotal = DB_INIC_8$CustoInicial + DB_INIC_8$CustoManutencao
      DB_INIC_8 = cbind(DB_INIC_8, CustoTotal)  
    } else {c(DB_INIC_8 <- NULL, DB_ARB_INIC_8 <- NULL)}
    
    ### INICIATIVA 9
    if(dataset_INIC_Selecao[9, 3] == TRUE){
      # Replicando as contantes por 90 anos
      dataset_Inic9_PrEventos_Obs = dataset_Inic9_PrEventos_Obs[rep(names(dataset_Inic9_PrEventos_Obs), 10)]
      dataset_Inic9_PrEventosRaros = dataset_Inic9_PrEventosRaros[rep(names(dataset_Inic9_PrEventosRaros), 10)]
      # Ajustando os nomes das colunas de todas as tabelas
      colnames(dataset_Inic9_PrEventos_Obs) <- anos_bd_init
      colnames(dataset_Inic9_PrEventosRaros) <- anos_bd_init
      colnames(dataset_INIC_Projetado) <- anos_bd_init
      colnames(dataset_Inic9_Custos) <- anos_bd_init
      
      DB_INIC_9 = rbind(dataset_INIC_Projetado,
                        dataset_Inic9_Custos,
                        dataset_Inic9_PrEventos_Obs,
                        dataset_Inic9_PrEventosRaros
      )
      
      DB_ARB_INIC_9 = rbind(dataset_Inic9_PrEventos_Arb,
                            dataset_Inic9_Ganhos)
      
      ### Calculo do Custo Total
      DB_INIC_9 = data.frame(t(DB_INIC_9))
      DB_ARB_INIC_9 = data.frame(t(DB_ARB_INIC_9))
      
      CustoTotal = DB_INIC_9$CustoInicial + DB_INIC_9$CustoManutencao
      DB_INIC_9 = cbind(DB_INIC_9, CustoTotal)  
    } else {c(DB_INIC_9 <- NULL, DB_ARB_INIC_9 <- NULL)}
    
    ### INICIATIVA 10
    if(dataset_INIC_Selecao[10, 3] == TRUE){
      # Replicando as contantes por 100 anos
      dataset_Inic10_PrEventos_Obs = dataset_Inic10_PrEventos_Obs[rep(names(dataset_Inic10_PrEventos_Obs), 10)]
      dataset_Inic10_PrEventosRaros = dataset_Inic10_PrEventosRaros[rep(names(dataset_Inic10_PrEventosRaros), 10)]
      # Ajustando os nomes das colunas de todas as tabelas
      colnames(dataset_Inic10_PrEventos_Obs) <- anos_bd_init
      colnames(dataset_Inic10_PrEventosRaros) <- anos_bd_init
      colnames(dataset_INIC_Projetado) <- anos_bd_init
      colnames(dataset_Inic10_Custos) <- anos_bd_init
      
      DB_INIC_10 = rbind(dataset_INIC_Projetado,
                         dataset_Inic10_Custos,
                         dataset_Inic10_PrEventos_Obs,
                         dataset_Inic10_PrEventosRaros
      )
      
      DB_ARB_INIC_10 = rbind(dataset_Inic10_PrEventos_Arb,
                             dataset_Inic10_Ganhos)
      
      ### Calculo do Custo Total
      DB_INIC_10 = data.frame(t(DB_INIC_10))
      DB_ARB_INIC_10 = data.frame(t(DB_ARB_INIC_10))
      
      CustoTotal = DB_INIC_10$CustoInicial + DB_INIC_10$CustoManutencao
      DB_INIC_10 = cbind(DB_INIC_10, CustoTotal)  
    } else {c(DB_INIC_10 <- NULL, DB_ARB_INIC_10 <- NULL)}

    #### 2.4 Sintese dos Objetos ####  

    # Agregando as variáveis arbitradas que estão somente no BD dos cenários o BD arbitrado ASIS
    # Gerando variáveis zeradas
    GanhoImagemReceitaEsperado = c(0.000001,0.00001,0.0000001)
    TFrMaximaImagem = c(0.000001,0.00001,0.0000001)
    TGrMaximaImagem  = c(0.000001,0.00001,0.0000001)
    GanhoProdutividade  = c(0.000001,0.00001,0.0000001)
    GanhoQualidade  = c(0.000001,0.00001,0.0000001)
    DespesasSeguroPatrimonial  = c(0.000001,0.00001,0.0000001)
    #PercPresenteismo  = rep(0, 4)
    #DespesasSeguroPatrimonial = rep(0, 4)
    
    var_INIT_ASIS = rbind(GanhoImagemReceitaEsperado,
                          TFrMaximaImagem,
                          TGrMaximaImagem,
                          GanhoProdutividade,
                          GanhoQualidade,
                          DespesasSeguroPatrimonial) 

    colnames(var_INIT_ASIS) <- c("Usual", "Máximo", "Mínimo")
    
    # Agregando variáveis ao BD AS IS
    DB_ASIS_Completo_Arbitrado = rbind(DB_ASIS_Completo_Arbitrado, 
                                       var_INIT_ASIS)
    
    showNotification("Dados importados processados, aguarde teste de consitência", type = c("warning"), duration = 3)

    #### 2.5 Teste consistência para garantir sanidade dos dados INICIATIVAS carregados pelo usuário <Criado em: jun/2018> ####    
    
    ### Log/Check 0 - Versão da planilha de entrada de dados - INICIATIVAS <Criado em: jun/2018>
    # Verifica se a versão da planilha é a mesma versão da planilha vigente
    versao_planilha_inic_vigente = "v1.0"
    # Testa se o objeto existe (campo esta preenchido na planilha)
    if(exists("versao_planilha_inic") & length(versao_planilha_inic)){
    if(versao_planilha_inic == versao_planilha_inic_vigente)
      {INIC_Logs = matrix(c("0", "Planilha", "Consistência versão planilha", "Ok", "Versão vigente"), nrow=1, ncol=5)} else 
      {INIC_Logs = matrix(c("0", "Planilha", "Consistência versão planilha", "Erro", "Atualizar os dados para versão vigente (download no menu ao lado)"), nrow=1, ncol=5)}} else {INIC_Logs = matrix(c("0", "Planilha", "Consistência versão planilha", "Erro", "Atualizar os dados para versão vigente (download no menu ao lado)"), nrow=1, ncol=5)}
    colnames(INIC_Logs) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")   
     
    ### Log/Check 01 - Preenchimento Planilha Parametrização INICIATIVA <Criado em: jun/2018>
    # Verifica o usuário selecionou a iniciativa, mas não preencheu o nome da mesma
    # Nro de descrições
    INIC_NRODESCR = sum(!is.na(dataset_INIC_Selecao[,2] ))
    # Nro de TRUES
    INIC_NROTRUE = sum(dataset_INIC_Selecao[,3], na.rm=TRUE)
    if(INIC_NRODESCR == INIC_NROTRUE)
      {INIC_Logs1 = matrix(c("1", "Parametrização", "Consistência descrição das iniciativas", "Ok", "Descrição das iniciativas estão ok"), nrow=1, ncol=5)} else
      {INIC_Logs1 = matrix(c("1", "Parametrização", "Consistência descrição das iniciativas", "Erro", "Existe(m) iniciativa(s) sem descrição"), nrow=1, ncol=5)}
    colnames(INIC_Logs1) = c("Id_log", "Aba Planilha Iniciativa", "Nome Teste", "Status", "Descrição Log")
    INIC_Logs = rbind(INIC_Logs, INIC_Logs1)
    
    ### Log/Check 02 - Preenchimento Custos das INICIATIVAS <Criado em: jun/2018>
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 1
    if(dataset_INIC_Selecao[1,3] == TRUE){
      if(is.na(dataset_Inic1_Custos[1,1]) || is.na(dataset_Inic1_Custos[2,1]) || is.na(dataset_Inic1_Custos[2,2]) || is.na(dataset_Inic1_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 1 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 2
    if(dataset_INIC_Selecao[2,3] == TRUE){
      if(is.na(dataset_Inic2_Custos[1,1]) || is.na(dataset_Inic2_Custos[2,1]) || is.na(dataset_Inic2_Custos[2,2]) || is.na(dataset_Inic2_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 2 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 3
    if(dataset_INIC_Selecao[3,3] == TRUE){
      if(is.na(dataset_Inic3_Custos[1,1]) || is.na(dataset_Inic3_Custos[2,1]) || is.na(dataset_Inic3_Custos[2,2]) || is.na(dataset_Inic3_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 3 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 4
    if(dataset_INIC_Selecao[4,3] == TRUE){
      if(is.na(dataset_Inic4_Custos[1,1]) || is.na(dataset_Inic4_Custos[2,1]) || is.na(dataset_Inic4_Custos[2,2]) || is.na(dataset_Inic4_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 4 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 5
    if(dataset_INIC_Selecao[5,3] == TRUE){
      if(is.na(dataset_Inic5_Custos[1,1]) || is.na(dataset_Inic5_Custos[2,1]) || is.na(dataset_Inic5_Custos[2,2]) || is.na(dataset_Inic5_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 5 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 6
    if(dataset_INIC_Selecao[6,3] == TRUE){
      if(is.na(dataset_Inic6_Custos[1,1]) || is.na(dataset_Inic6_Custos[2,1]) || is.na(dataset_Inic6_Custos[2,2]) || is.na(dataset_Inic6_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 6 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 7
    if(dataset_INIC_Selecao[7,3] == TRUE){
      if(is.na(dataset_Inic7_Custos[1,1]) || is.na(dataset_Inic7_Custos[2,1]) || is.na(dataset_Inic7_Custos[2,2]) || is.na(dataset_Inic7_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 7 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 8
    if(dataset_INIC_Selecao[8,3] == TRUE){
      if(is.na(dataset_Inic8_Custos[1,1]) || is.na(dataset_Inic8_Custos[2,1]) || is.na(dataset_Inic8_Custos[2,2]) || is.na(dataset_Inic8_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 8 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 9
    if(dataset_INIC_Selecao[9,3] == TRUE){
      if(is.na(dataset_Inic9_Custos[1,1]) || is.na(dataset_Inic9_Custos[2,1]) || is.na(dataset_Inic9_Custos[2,2]) || is.na(dataset_Inic9_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 9 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Testar para cada iniciativa se os custos foram preenchidos
    # Custo Iniciativa 10
    if(dataset_INIC_Selecao[10,3] == TRUE){
      if(is.na(dataset_Inic10_Custos[1,1]) || is.na(dataset_Inic10_Custos[2,1]) || is.na(dataset_Inic10_Custos[2,2]) || is.na(dataset_Inic10_Custos[2,3])){
        INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Erro", "Custos (investimento ou manutenção) iniciativa 10 não preenchidos. Preencher 4 anos"), nrow=1, ncol=5); colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs2)}}
    # Caso todos os testes de preenchimento não apresentar erro, informar no quadro de log
    if(length(INIC_Logs[,1][INIC_Logs[,1] == 2]) == 0){
      INIC_Logs2 = matrix(c("2", "Custos Iniciativas", "Consistência preenchimentos custos", "Ok", "Preenchimento dos custos das iniciativas está adequado"), nrow=1, ncol=5)
      colnames(INIC_Logs2) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
      INIC_Logs = rbind(INIC_Logs, INIC_Logs2)
    }

    ### Log/Check 03 - Preenchimento eventos (número e prob. AsIs) das INICIATIVAS <Criado em: jun/2018>
    # Testar se as probabilidades e nro de eventos ASIS foram preenchidas corretamente
    # Criação de objeto com nro eventos e prob eventos do ultimo ano e da média
    Check_Eventos_ASIS = data.frame(cbind(eventos_pdf[,6:7], eventos_pdf_prob[,6:7]))
    # Trunca os dados para 6 casas decimais
    Check_Eventos_ASIS = round(Check_Eventos_ASIS, digits = 6)
    if(dataset_INIC_Selecao[1,3] == TRUE) {dataset_Inic1_EventosASIS = round(dataset_Inic1_EventosASIS, digits = 6)}
    if(dataset_INIC_Selecao[2,3] == TRUE) {dataset_Inic2_EventosASIS = round(dataset_Inic2_EventosASIS, digits = 6)}
    if(dataset_INIC_Selecao[3,3] == TRUE) {dataset_Inic3_EventosASIS = round(dataset_Inic3_EventosASIS, digits = 6)}
    if(dataset_INIC_Selecao[4,3] == TRUE) {dataset_Inic4_EventosASIS = round(dataset_Inic4_EventosASIS, digits = 6)}
    if(dataset_INIC_Selecao[5,3] == TRUE) {dataset_Inic5_EventosASIS = round(dataset_Inic5_EventosASIS, digits = 6)}
    if(dataset_INIC_Selecao[6,3] == TRUE) {dataset_Inic6_EventosASIS = round(dataset_Inic6_EventosASIS, digits = 6)}
    if(dataset_INIC_Selecao[7,3] == TRUE) {dataset_Inic7_EventosASIS = round(dataset_Inic7_EventosASIS, digits = 6)}
    if(dataset_INIC_Selecao[8,3] == TRUE) {dataset_Inic8_EventosASIS = round(dataset_Inic8_EventosASIS, digits = 6)}
    if(dataset_INIC_Selecao[9,3] == TRUE) {dataset_Inic9_EventosASIS = round(dataset_Inic9_EventosASIS, digits = 6)}
    if(dataset_INIC_Selecao[10,3] == TRUE) {dataset_Inic10_EventosASIS = round(dataset_Inic10_EventosASIS, digits = 6)}
    
    # Eventos ASIS - Baseline Iniciativa 1
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[1,3] == TRUE){
     if(any(Check_Eventos_ASIS != dataset_Inic1_EventosASIS)){
       INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 1 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[2,3] == TRUE){
      if(any(Check_Eventos_ASIS != dataset_Inic2_EventosASIS)){
        INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 2 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[3,3] == TRUE){
      if(any(Check_Eventos_ASIS != dataset_Inic3_EventosASIS)){
        INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 3 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[4,3] == TRUE){
      if(any(Check_Eventos_ASIS != dataset_Inic4_EventosASIS)){
        INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 4 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[5,3] == TRUE){
      if(any(Check_Eventos_ASIS != dataset_Inic5_EventosASIS)){
        INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 5 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[6,3] == TRUE){
      if(any(Check_Eventos_ASIS != dataset_Inic6_EventosASIS)){
        INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 6 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[7,3] == TRUE){
      if(any(Check_Eventos_ASIS != dataset_Inic7_EventosASIS)){
        INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 7 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[8,3] == TRUE){
      if(any(Check_Eventos_ASIS != dataset_Inic8_EventosASIS)){
        INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 8 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[9,3] == TRUE){
      if(any(Check_Eventos_ASIS != dataset_Inic9_EventosASIS)){
        INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 9 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Testa se todos os valores são identicos. Se pelo um for diferente erro.
    if(dataset_INIC_Selecao[10,3] == TRUE){
      if(any(Check_Eventos_ASIS != dataset_Inic10_EventosASIS)){
        INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Erro", "Eventos (número ou probabilidade) iniciativa 19 preenchidos incorretamente. Copiar valores do arquivo csv"), nrow=1, ncol=5); colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log");  INIC_Logs = rbind(INIC_Logs, INIC_Logs3)}}
    # Caso todos os testes de preenchimento não apresentar erro, informar no quadro de log
    if(length(INIC_Logs[,1][INIC_Logs[,1] == 3]) == 0){
     INIC_Logs3 = matrix(c("3", "Eventos AsIS Iniciativas", "Consistência preenchimentos Eventos AsIs", "Ok", "Preenchimento dos eventos AsIs das iniciativas está adequado"), nrow=1, ncol=5)
     colnames(INIC_Logs3) = c("Id_log", "Aba Planilha", "Nome Teste", "Status","Descrição Log")
     INIC_Logs = rbind(INIC_Logs, INIC_Logs3)
    }

    # Salvar arquivos para debugar 
    write.csv2(DB_Calc_stats,"Outputs/DB_Calc_stats.csv")
    write.csv2(DB_ASIS_Completo_Arbitrado,"Outputs/DB_ASIS_Completo_Arbitrado.csv")
    write.csv2(dataset_INIC_BASELINE,"Outputs/dataset_INIC_BASELINE.csv")
    
    # Flag indica que dados ASIS foram processados
    Flag_Iniciativas = 1
    
    showNotification("Teste de consitência finalizado. Se não houver erro, pronto para rodar a calculadora", type = c("warning"), duration = 3)
    
    #### 2.6 Lista de objetos para serem utilizados em outras funções reativas ####
    INIT_List <- list(
      INIC_Logs = INIC_Logs,
      Flag_Iniciativas = Flag_Iniciativas,
      dataset_ASIS_param_Modulos =  dataset_ASIS_param_Modulos,
      dataset_INIC_Selecao =  dataset_INIC_Selecao,
      dataset_INIC_BASELINE =  dataset_INIC_BASELINE,
      dataset_Inic1_Custos =  dataset_Inic1_Custos,
      dataset_Inic2_Custos =  dataset_Inic2_Custos,
      dataset_Inic3_Custos =  dataset_Inic3_Custos,
      dataset_Inic4_Custos =  dataset_Inic4_Custos,
      dataset_Inic5_Custos =  dataset_Inic5_Custos,
      dataset_Inic6_Custos =  dataset_Inic6_Custos,
      dataset_Inic7_Custos =  dataset_Inic7_Custos,
      dataset_Inic8_Custos =  dataset_Inic8_Custos,
      dataset_Inic9_Custos =  dataset_Inic9_Custos,
      dataset_Inic10_Custos =  dataset_Inic10_Custos,
      dataset_ASIS_param_taxadesconto =  dataset_ASIS_param_taxadesconto,
      dataset_ASIS_param_cadastEmp =  dataset_ASIS_param_cadastEmp,
      dataset_INIC_AnosAvaliacao =  dataset_INIC_AnosAvaliacao,
      dataset_INIC_Projetado =  dataset_INIC_Projetado,
      DB_Calc_stats =  DB_Calc_stats,
      DB_ASIS_Completo_Arbitrado =  DB_ASIS_Completo_Arbitrado,
      DB_INIC_1 =  DB_INIC_1,
      DB_ARB_INIC_1 =  DB_ARB_INIC_1,
      DB_INIC_2 =  DB_INIC_2,
      DB_ARB_INIC_2 =  DB_ARB_INIC_2,
      DB_INIC_3 =  DB_INIC_3,
      DB_ARB_INIC_3 =  DB_ARB_INIC_3,
      DB_INIC_4 =  DB_INIC_4,
      DB_ARB_INIC_4 =  DB_ARB_INIC_4,
      DB_INIC_5 =  DB_INIC_5,
      DB_ARB_INIC_5 =  DB_ARB_INIC_5,
      DB_INIC_6 =  DB_INIC_6,
      DB_ARB_INIC_6 =  DB_ARB_INIC_6,
      DB_INIC_7 =  DB_INIC_7,
      DB_ARB_INIC_7 =  DB_ARB_INIC_7,
      DB_INIC_8 =  DB_INIC_8,
      DB_ARB_INIC_8 =  DB_ARB_INIC_8,
      DB_INIC_9 =  DB_INIC_9,
      DB_ARB_INIC_9 =  DB_ARB_INIC_9,
      DB_INIC_10 =  DB_INIC_10,
      DB_ARB_INIC_10 =  DB_ARB_INIC_10
    ) 

  }) # Fim função reativa Parte 2
  
  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  # Parte 3: Rodar a Calculadora -----
  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  parte3 <- reactive({

    #### 3.1 Carregar os Dados ####

    # Carregar objetos gerados na leitura e processamentos dos dados ASIS (Parte1)
    INIT_List1 <- parte2()
    dataset_ASIS_param_Modulos <- INIT_List1$ dataset_ASIS_param_Modulos
    dataset_INIC_Selecao <- INIT_List1$ dataset_INIC_Selecao
    dataset_INIC_BASELINE <- INIT_List1$ dataset_INIC_BASELINE
    dataset_Inic1_Custos <- INIT_List1$ dataset_Inic1_Custos
    dataset_Inic2_Custos <- INIT_List1$ dataset_Inic2_Custos
    dataset_Inic3_Custos <- INIT_List1$ dataset_Inic3_Custos
    dataset_Inic4_Custos <- INIT_List1$ dataset_Inic4_Custos
    dataset_Inic5_Custos <- INIT_List1$ dataset_Inic5_Custos
    dataset_Inic6_Custos <- INIT_List1$ dataset_Inic6_Custos
    dataset_Inic7_Custos <- INIT_List1$ dataset_Inic7_Custos
    dataset_Inic8_Custos <- INIT_List1$ dataset_Inic8_Custos
    dataset_Inic9_Custos <- INIT_List1$ dataset_Inic9_Custos
    dataset_Inic10_Custos <- INIT_List1$ dataset_Inic10_Custos
    dataset_ASIS_param_taxadesconto <- INIT_List1$ dataset_ASIS_param_taxadesconto
    dataset_ASIS_param_cadastEmp <- INIT_List1$ dataset_ASIS_param_cadastEmp
    dataset_INIC_AnosAvaliacao <- INIT_List1$ dataset_INIC_AnosAvaliacao
    dataset_INIC_Projetado <- INIT_List1$ dataset_INIC_Projetado
    DB_Calc_stats <- INIT_List1$ DB_Calc_stats
    DB_ASIS_Completo_Arbitrado <- INIT_List1$ DB_ASIS_Completo_Arbitrado
    DB_INIC_1 <- INIT_List1$ DB_INIC_1
    DB_ARB_INIC_1 <- INIT_List1$ DB_ARB_INIC_1
    DB_INIC_2 <- INIT_List1$ DB_INIC_2
    DB_ARB_INIC_2 <- INIT_List1$ DB_ARB_INIC_2
    DB_INIC_3 <- INIT_List1$ DB_INIC_3
    DB_ARB_INIC_3 <- INIT_List1$ DB_ARB_INIC_3
    DB_INIC_4 <- INIT_List1$ DB_INIC_4
    DB_ARB_INIC_4 <- INIT_List1$ DB_ARB_INIC_4
    DB_INIC_5 <- INIT_List1$ DB_INIC_5
    DB_ARB_INIC_5 <- INIT_List1$ DB_ARB_INIC_5
    DB_INIC_6 <- INIT_List1$ DB_INIC_6
    DB_ARB_INIC_6 <- INIT_List1$ DB_ARB_INIC_6
    DB_INIC_7 <- INIT_List1$ DB_INIC_7
    DB_ARB_INIC_7 <- INIT_List1$ DB_ARB_INIC_7
    DB_INIC_8 <- INIT_List1$ DB_INIC_8
    DB_ARB_INIC_8 <- INIT_List1$ DB_ARB_INIC_8
    DB_INIC_9 <- INIT_List1$ DB_INIC_9
    DB_ARB_INIC_9 <- INIT_List1$ DB_ARB_INIC_9
    DB_INIC_10 <- INIT_List1$ DB_INIC_10
    DB_ARB_INIC_10 <- INIT_List1$ DB_ARB_INIC_10
    
    showNotification("Iniciando os cálculos da calculadora. Aguarde. Pode levar alguns minutos", type = c("warning"), duration = 20)

    #### 3.2 Organizar os dados para o formato desejado pela calculadora ####  
    
    # Se o log não foi incializado, inicializar o log.
    if(!exists("oshcba.log_calculadora")){
      oshcba.iniciar_log()
    }
    
    dadostratados = tryCatch(
      {
        dadostratados = list(
          Modulos = dataset_ASIS_param_Modulos,
          Cenarios = dataset_INIC_Selecao,
          Baseline = dataset_INIC_BASELINE,
          Custos = list(Iniciativa1 = dataset_Inic1_Custos,
                        Iniciativa2 = dataset_Inic2_Custos,
                        Iniciativa3 = dataset_Inic3_Custos,
                        Iniciativa4 = dataset_Inic4_Custos,
                        Iniciativa5 = dataset_Inic5_Custos,
                        Iniciativa6 = dataset_Inic6_Custos,
                        Iniciativa7 = dataset_Inic7_Custos,
                        Iniciativa8 = dataset_Inic8_Custos,
                        Iniciativa9 = dataset_Inic9_Custos,
                        Iniciativa10 = dataset_Inic10_Custos
          ),
          Configs = list(TaxaDesconto = dataset_ASIS_param_taxadesconto,
                         CadastroEmpresa = dataset_ASIS_param_cadastEmp,
                         AnosASimular = dataset_INIC_AnosAvaliacao),
          DadosProjetados = dataset_INIC_Projetado,
          DadosObservados = DB_Calc_stats,
          DadosArbitrados = DB_ASIS_Completo_Arbitrado,
          DadosObservadosInic1 = DB_INIC_1,
          DadosArbitradosInic1 = DB_ARB_INIC_1,
          DadosObservadosInic2 = DB_INIC_2,
          DadosArbitradosInic2 = DB_ARB_INIC_2,
          DadosObservadosInic3 = DB_INIC_3,
          DadosArbitradosInic3 = DB_ARB_INIC_3,
          DadosObservadosInic4 = DB_INIC_4,
          DadosArbitradosInic4 = DB_ARB_INIC_4,
          DadosObservadosInic5 = DB_INIC_5,
          DadosArbitradosInic5 = DB_ARB_INIC_5,
          DadosObservadosInic6 = DB_INIC_6,
          DadosArbitradosInic6 = DB_ARB_INIC_6,
          DadosObservadosInic7 = DB_INIC_7,
          DadosArbitradosInic7 = DB_ARB_INIC_7,
          DadosObservadosInic8 = DB_INIC_8,
          DadosArbitradosInic8 = DB_ARB_INIC_8,
          DadosObservadosInic9 = DB_INIC_9,
          DadosArbitradosInic9 = DB_ARB_INIC_9,
          DadosObservadosInic10 = DB_INIC_10,
          DadosArbitradosInic10 = DB_ARB_INIC_10
        )
      },
      error = function(cond){
        oshcba.adicionar_log("Erro: Nem todos os dados do tratamento de dados estão disponiveis.")
        oshcba.adicionar_log(cond)
        oshcba.parar_execucao("Excução Interrompida por erro nos arquivos de dados.")
      })

    #### 3.3 Rodar a Calculadora ####  
    list_inputs = list_inputs = oshcba::obter_inputs_list_dados_tratados(list_dados_tratados = dadostratados)
    
    # Salvar tabelas pós processamento para input na calculadora
    write.csv2(list_inputs$Configs,"Outputs/InputCalc_Configs.csv")
    write.csv2(list_inputs$DadosProjetados,"Outputs/InputCalc_DadosProjetados.csv")
    write.csv2(list_inputs$Parametros,"Outputs/InputCalc_Parametros.csv")
    write.csv2(list_inputs$Cenarios,"Outputs/InputCalc_Cenarios.csv")
    write.csv2(list_inputs$Custos,"Outputs/InputCalc_Custos.csv")
    write.csv2(list_inputs$HistoricoFAP,"Outputs/InputCalc_HistoricoFAP.csv")
    write.csv2(list_inputs$Modulos,"Outputs/InputCalc_Modulos.csv")
    write.csv2(list_inputs$Constantes,"Outputs/InputCalc_Constantes.csv")
    
    # Desligando módulos manualmente:
    verificar_inputs(list_inputs)
    
    # Desligar Módulos Manualmente
    # modulos_a_desligar = c("calcular_refugo_retrabalho", "calcular_engajamento", "calcular_qualidade", "calcular_imagem_receita", "calcular_imagem_contracacao", "calcular_interrupcao_acidentes")
    # modulos_a_desligar = c()
    # list_inputs$Modulos$Calcular = !(list_inputs$Modulos$Modulo %in% modulos_a_desligar)
    
    # Onde a Distribuição for normaltruncada e o desvio for zero, usar a normal ao invés de normal truncada.
    resultados = simular_cba(ArquivoInputs = list_inputs , tipo_input = "list")
    
    # Salvar arquivos para debugar 
    write.csv2(resultados$Resultados_Descontados,"Outputs/Output_Resultados_Descontados.csv")
    write.csv2(resultados$Resultados_CBR,"Outputs/Output_Resultados_CBR.csv")
    
    showNotification("Quase pronto! Gerando os arquivos relatórios", type = c("warning"), duration = 10)
    
    rmarkdown::render("relatorio_logs.Rmd", encoding = "UTF-8")
    rmarkdown::render("relatorio.Rmd", encoding = "UTF-8")

    showNotification("Pronto!", type = c("warning"), duration = 10)
    
    # Sintese dos resultados da calculadora para apresentar em tela
    outputcalc = resumo_cba_por_iniciativa(resultados$Resultados_CBR)[,1:4]

    CALC_List <- list(
      outputcalc = outputcalc
    )

  }) # Fim da parte 3
  
  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  # Parte 4: Setup dos botões e objetos reativos de apresentação de resultados -----
  #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  # Incia o cálculo da calculadora quando o usuário pressiona o botão de rodar
  observeEvent(input$RodarCalculadorabtn, {
    parte3()
  })
  
  # Mostra botão rodar caluladora somente após carregar os dados iniciativas
  observe({
    if(is.null(input$file3)){
      shinyjs::hide("RodarCalculadorabtn")}
    if(!is.null(input$file3)){
      INIC_Logs3 <- parte2()
      Flag_Iniciativas<-INIC_Logs3$Flag_Iniciativas
      if(Flag_Iniciativas == 1){
        shinyjs::show("RodarCalculadorabtn")}
    }
  })
  
  # Mostra botão para geração do arquivo pdf (relatório ASIS) e botões de download dos arquivos somente após dados ASIS carregados 
  observe({
    if(is.null(input$file1)){
      shinyjs::hide("makepdf1")
      shinyjs::hide("downloadData1")
      shinyjs::hide("downloadData1b")
      shinyjs::hide("downloadData1c")}
    if(!is.null(input$file1)){
      ASIS_List3 <- parte1()
      Flag_asis<-ASIS_List3$Flag_asis
      if(Flag_asis == 1){
        shinyjs::show("makepdf1")
        shinyjs::show("downloadData1")
        shinyjs::show("downloadData1b")
        shinyjs::show("downloadData1c")}
    }
  })

  # Botão para geração do arquivo pdf - Relatório.pdf
  observeEvent(input$makepdf1, {
    showNotification("Gerando pdf, aguarde", type = c("warning"), duration = 2)
    ASIS_List2 <- parte1()
    dataset_ASIS_param_cadastEmp<-ASIS_List2$dataset_ASIS_param_cadastEmp
    dados_inss<-ASIS_List2$dados_inss
    dataset_ASIS_param_Modulos<-ASIS_List2$dataset_ASIS_param_Modulos
    check_variaveis1<-ASIS_List2$check_variaveis1
    eventos_pdf<-ASIS_List2$eventos_pdf
    eventos_pdf_prob<-ASIS_List2$eventos_pdf_prob
    eventos_pdf_arb<-ASIS_List2$eventos_pdf_arb
    rmarkdown::render("report.Rmd", encoding = "UTF-8")
    showNotification("Pdf gerado. Pode ser baixado", type = c("warning"), duration = 2)
  })

  ## Download pdf ASIS
  output$downloadData1 <- downloadHandler(
    filename = "report.pdf",
    content = function(file) {
      file.copy(from = "report.pdf", to = file)
    })

  ### Download csv nro eventos ASIS
  output$downloadData1b <- downloadHandler(
    filename = "Nro_Eventos.csv",
    content = function(file) {
      file.copy("Outputs/Nro_Eventos.csv", file)
    })
  
  ### Download csv prob. eventos ASIS
  output$downloadData1c <- downloadHandler(
    filename = "Prob_Eventos.csv",
    content = function(file) {
      file.copy("Outputs/Prob_Eventos.csv", file)
    })
  
  ### Download pdf ROI 
  output$downloadData2 <- downloadHandler(
    filename = "relatorio.pdf",
    content = function(file2) {
      file.copy("relatorio.pdf", file2)
    })
  
  ### Download pdf ROI com logs
  output$downloadData_logs <- downloadHandler(
    filename = "relatorio_logs.pdf",
    content = function(file) {
      file.copy("relatorio_logs.pdf", file)
    })
  
  ### Print resultado da calculadora   
  output$output_calculadora <- DT::renderDataTable({
    if (is.null(input$file3)){
      return(data.frame(c("Dados ainda não foram carregados")))
    }
    else {
      CALC_List2 <- parte3()
      outputcalc <- CALC_List2$outputcalc
      return(outputcalc)
    }
  })   

  ### Print Logs análises dados ASIS  
  output$outputTable_AsisLogs <- DT::renderDataTable({
    if (is.null(input$file1)){
      return(data.frame(c("Dados não foram carregados")))
    }
    else {
      ASIS_List1 <- parte1()
      LogsAsIs <- ASIS_List1$AsIS_Logs
      return(LogsAsIs)
    }
  })   
  
  ### Print Logs análises dados INIT  
  output$outputTable_INITLogs <- DT::renderDataTable({
    if (is.null(input$file3)){
      return(data.frame(c("Dados não foram carregados")))
    }
    else {
      INIT_List2 <- parte2()
      INICLogs <- INIT_List2$INIC_Logs
      return(INICLogs)
    }
  })   
  
  ### Quadro informando status dos testes antes de rodar a calculadora 
  output$outputTable_StatusCalc <- DT::renderDataTable({
    if (is.null(input$file3)){return()}
    else {
      ASIS_List1 <- parte1()
      LogsAsIs <- ASIS_List1$AsIS_Logs
      INIT_List3 <- parte2()
      INICLogs <- INIT_List3$INIC_Logs
      
      if(any(LogsAsIs == "Erro")){status_logs_asis = matrix(c("Dados AsIs", "Rever os dados AsiS (erros) antes de rodar a calculadora"), nrow=1,ncol=2)} else {status_logs_asis = matrix(c("Dados AsIs", "Dados AsiS ok"),nrow=1,ncol=2)}
      if(any(INICLogs == "Erro")){status_logs_init = matrix(c("Dados Iniciativas", "Rever os dados Inic. (erros) antes de rodar a calculadora"), nrow=1,ncol=2)} else {status_logs_init = matrix(c("Dados Iniciativas", "Dados Inic. ok"), nrow=1,ncol=2)}
      
      colnames(status_logs_asis) <- c("Etapa", "Status")
      colnames(status_logs_init) <- c("Etapa", "Status")
      
      Logs_dados_entrada = rbind(status_logs_asis, status_logs_init)
      
      return(Logs_dados_entrada)
    }
  }) 
  
  ### Download Template Excel ASIS
  output$excelasis <- downloadHandler(
    filename = "DOC012- Planilha dados de entrada.xlsx",
    content = function(file) {
      file.copy("PlanilhasEntrada/DOC012- Planilha dados de entrada.xlsx", file)
    })
  
  ### Download Template Excel TOBE
  output$excelinit <- downloadHandler(
    filename = "DOC013-Planilha Iniciativas.xlsx",
    content = function(file) {
      file.copy("PlanilhasEntrada/DOC013-Planilha Iniciativas.xlsx", file)
    })

})  #END shinyServer
